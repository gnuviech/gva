Development ideas and planned features
======================================

* add pure redirect websites
* add management for rewrite rules
* add accounts without SFTP (for pure mail hosting)
* allow generation of Key and CSR, add upload of certificates for HTTPS sites
* add XMPP management
