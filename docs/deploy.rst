Deploy
======

The production deployment for gnuviechadmin is performed using saltstack and
consists of the following steps:

* installation of native dependencies
* setup of a virtualenv
* installation of gnuviechadmin production dependencies inside the virtualenv
* setup of uwsgi application for the web interface
* setup of nginx with certificates and UWSGI support
