:py:mod:`dashboard` app
=======================

.. automodule:: dashboard


:py:mod:`models <dashboard.models>`
-----------------------------------

.. automodule:: dashboard.models


:py:mod:`urls <dashboard.urls>`
-------------------------------

.. automodule:: dashboard.urls


:py:mod:`views <dashboard.views>`
---------------------------------

.. automodule:: dashboard.views
