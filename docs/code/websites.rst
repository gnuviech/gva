:py:mod:`websites` app
======================

.. automodule:: websites


:py:mod:`admin <websites.admin>`
--------------------------------

.. automodule:: websites.admin
   :members:


:py:mod:`apps <websites.apps>`
------------------------------

.. automodule:: websites.apps
   :members:


:py:mod:`forms <websites.forms>`
--------------------------------

.. automodule:: websites.forms
   :members:


:py:mod:`models <websites.models>`
----------------------------------

.. automodule:: websites.models
   :members:


:py:mod:`urls <websites.urls>`
------------------------------

.. automodule:: websites.urls
   :members:


:py:mod:`views <websites.views>`
--------------------------------

.. automodule:: websites.views
   :members:
