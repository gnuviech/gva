:py:mod:`gvacommon`
===================

This module is imported from a separate git project via git subtree and
provides some functionality that is common to all gnuviechadmin subprojects.

.. automodule:: gvacommon


:py:mod:`celeryrouters <gvacommon.celeryrouters>`
-------------------------------------------------

.. automodule:: gvacommon.celeryrouters
   :members:
   :undoc-members:


:py:mod:`viewmixins <gvacommon.viewmixins>`
-------------------------------------------

.. automodule:: gvacommon.viewmixins
   :members:
   :undoc-members:
