:py:mod:`taskresults` app
=========================

.. automodule:: taskresults

:py:mod:`admin <taskresults.admin>`
-----------------------------------

.. automodule:: taskresults.admin

:py:mod:`management.commands <taskresults.management.commands>`
---------------------------------------------------------------

.. automodule:: taskresults.management.commands

:py:mod:`fetch_taskresults <taskresult.management.commands.fetch_taskresults>`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: taskresults.management.commands.fetch_taskresults

:py:mod:`models <taskresults.models>`
-------------------------------------

.. automodule:: taskresults.models
