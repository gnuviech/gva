:py:mod:`fileservertasks` app
=============================

.. automodule:: fileservertasks


:py:mod:`tasks <fileservertasks.tasks>`
---------------------------------------

.. automodule:: fileservertasks.tasks
   :members:
   :undoc-members:
