:py:mod:`managemails` app
=========================

.. automodule:: managemails


:py:mod:`admin <managemails.admin>`
-----------------------------------

.. automodule:: managemails.admin
   :members:


:py:mod:`apps <managemails.apps>`
---------------------------------

.. automodule:: managemails.apps
   :members:


:py:mod:`forms <managemails.forms>`
-----------------------------------

.. automodule:: managemails.forms
   :members:


:py:mod:`models <managemails.models>`
-------------------------------------

.. automodule:: managemails.models
   :members:


:py:mod:`urls <managemails.urls>`
---------------------------------

.. automodule:: managemails.urls
   :members:


:py:mod:`views <managemails.views>`
-----------------------------------

.. automodule:: managemails.views
   :members:
