:py:mod:`gvawebcore`
====================

.. automodule:: gvawebcore


:py:mod:`forms <gvawebcore.forms>`
----------------------------------

.. automodule:: gvawebcore.forms
   :members:


:py:mod:`views <gvawebcore.views>`
----------------------------------

.. automodule:: gvawebcore.views
   :members:
