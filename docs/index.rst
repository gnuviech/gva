.. gnuviechadmin documentation master file, created by
   sphinx-quickstart on Sun Feb 17 11:46:20 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================================
Welcome to gnuviechadmin's documentation!
=========================================

.. include:: ../README.rst

Contents
--------

.. toctree::
   :maxdepth: 3

   install
   deploy
   tests
   code
   ideas
   task_flows
   changelog

License
-------

gnuviechadmin is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

.. include:: ../COPYING
   :literal:


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
