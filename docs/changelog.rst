Changelog
=========

* :release:`0.15.1 <2023-07-23>
* :bug:`-` remove stale disk usage stats older than 30 minutes

* :release:`0.15.0 <2023-07-23>
* :feature:`10` add disk usage details for mail and web

* :release:`0.14.4 <2023-07-22>`
* :bug:`-` add customer to disk space detail view

* :release:`0.14.3 <2023-07-22>`
* :bug:`-` fix missing permission check on disk space detail view

* :release:`0.14.2 <2023-07-22>`
* :bug:`-` fix division by zero for hosting packages without disk space allocation

* :release:`0.14.1 <2023-07-22>`
* :bug:`-` fix squashed migration for disk space statistics

* :release:`0.14.0 <2023-07-22>`
* :feature:`-` add disk space statistics

* :release:`0.13.0 <2023-05-08>`
* :feature:`-` add REST API to retrieve and set user information as admin
* :feature:`-` add support model for offline account reset codes in new help
  app
* :support:`-` remove unused PowerDNS support tables from domains app
* :feature:`-` add impersonation support for superusers
* :support:`-` remove django-braces dependency
* :support:`-` remove Twitter support
* :support:`-` update dependencies

* :release:`0.12.1 <2020-04-13>`
* :bug:`7` fix handling of undefined mail domains in customer hosting package
  detail template

* :release:`0.12.0 <2020-04-10>`
* :support:`-` add architecture diagramm for documentation
* :support:`-` drop environment specific settings
* :support:`-` update to Python 3
* :support:`-` use Pipenv for dependency management
* :support:`-` switch result backend to Redis
* :support:`-` use separate test vhost for celery queues
* :support:`-` switch licensing to AGPLv3+
* :support:`-` add a Vagrant setup to ease development
* :support:`-` add Docker setup for local development 
* :feature:`-` let all celery tasks run asynchronously and move task processing
  to signal handlers
* :feature:`-` add unit tests for all the code
* :feature:`-` add proper configuration for coverage, flake8 and pep8
* :feature:`-` update to Django 2.2.12
* :support:`-` use gvacommon from separate repository
* :support:`-` update documentation

* :release:`0.11.6 <2020-02-14>`
* :support:`-` Update dependencies to versions that work with Debian Stretch

* :release:`0.11.5 <2018-12-26>`
* :support:`-` Remove Xing support from settings and templates

* :release:`0.11.4 <2016-12-31>`
* :bug:`-` fix wrong tag in password reset done template

* :release:`0.11.3 <2015-02-21>`
* :bug:`-` fix handling of OpenSSH formatted keys with whitespace in comments
* :bug:`-` the ssh key list does not show SSH keys of other users anymore

* :release:`0.11.2 <2015-02-06>`
* :bug:`-` fix wrong variable name in
  managemails.models.MailAddress.set_forward_addresses and typo in
  managemails.forms.EditMailAddressForm

* :release:`0.11.1 <2015-02-01>`
* :bug:`-` fix translation of contact form by using ugettext_lazy and adding
  contact_form to INSTALLED_APPS

* :release:`0.11.0 <2015-02-01>`
* :feature:`-` add icons to top level navigation
* :feature:`-` add contact form
* :feature:`-` add imprint as flatpage
* :support:`-` mark active menu item as active via context_processor and
  corresponding template markup
* :feature:`-` add links to webmail, phpmyadmin and phppgadmin

* :release:`0.10.0 <2015-02-01>`
* :support:`-` move taskresults tests to tasksresults.tests and fix them
* :support:`-` cache result of get_hosting_package method of
  gvawebcore.views.HostingPackageAndCustomerMixin to improve page loading
  performance
* :feature:`-` add ability to add, list and delete SSH public keys assigned to
  a hosting package's operating system user and change their comments
* :feature:`-` add ability to add SSH public keys for operating system users
* :support:`-` make tests in osusers.tests work again
* :support:`-` minor HTML improvements
* :support:`-` add API for gvafile task set_file_ssh_authorized_keys (requires
  gvafile >= 0.5.0 on the fileserver side)
* :support:`-` update to Django 1.7.4

* :release:`0.9.0 <2015-01-27>`
* :feature:`-` setup nginx virtual host and PHP configuration for websites
  (requires gvaweb >= 0.1.0 on web server)
* :support:`-` implement domain name validation
* :feature:`-` implement deletion of websites
* :feature:`-` implement setup of websites
* :support:`-` add webtasks interface
* :support:`-` update to new fileservertasks interface (requires gvafile >=
  0.4.0 on the fileserver)

* :release:`0.8.0 <2015-01-26>`
* :feature:`-` implement deletion of user database and database users
* :feature:`-` implement password changes for database users
* :feature:`-` implement setup of user databases
* :support:`-` performance improvement for hosting package detail view
* :support:`-` move HostingPackageAndCustomerMixin from managemails.views to
  gvawebcore.views

* :release:`0.7.0 <2015-01-25>`
* :feature:`-` implement mail address target editing
* :feature:`-` implement mail address deletion
* :feature:`-` implement adding mail address to mail domains
* :feature:`-` implement adding options to hosting packages
* :bug:`- major` fix disk space calculation in
  hostingpackages.models.CustomerHostingPackage
* :bug:`- major` fix unique constraints on
  hostingpackages.models.CustomerDiskSpaceOption and
  hostingpackages.models.CustomerDatabaseOption
* :feature:`-` implement password change functionality for mailboxes
* :feature:`-` implement creation of new mailboxes for hosting packages
* :support:`-` move common form code to new module gvawebcore.forms
* :feature:`-` make it possible to assign domains to a customer
* :feature:`-` add hosting packages list for staff users
* :feature:`-` allow creation of new hosting packages for staff users without
  the need to navigate to a customer dashboard first

* :release:`0.6.0 <2015-01-24>`
* :feature:`-` add frontend functionality to set an os users' sftp password
  (needs gvaldap >= 0.4.0 on the LDAP side)
* :support:`-` remove unused dashboard.views.LogoutView and the corresponding
  URL in dashboard.urls
* :feature:`-` add new task stub to set an ldap user's password
* :support:`-` refactor osusers.tasks, split into fileservertasks.tasks and
  ldaptasks.tasks
* :feature:`-` show hosting package information on user dashboard
* :feature:`-` implement new hostingpackages app to provide hosting package
  templates, hosting options and customer hosting packages as well as customer
  specific hosting package options
* :feature:`-` add template tags for database icons and human readable names in
  :py:mod:`userdbs.templatetags.userdb`

* :release:`0.5.2 <2015-01-18>`
* :bug:`-` define proper allauth production settings with https and mandatory
  email verification

* :release:`0.5.1 <2015-01-18>`
* :bug:`-` load jquery and html5 with same URL schema as the rest of the site

* :release:`0.5.0 <2015-01-17>`
* :feature:`-` add authentication via social media accounts from Google,
  LinkedIn, Twitter and Xing
* :feature:`-` add customer login/logout and dashboard templates
* :feature:`-` add admin list filtering and ordering for mail addresses and
  mailboxes

* :release:`0.4.0 <2015-01-11>`
* :feature:`-` add mysqltasks and pgsqltasks
* :feature:`-` add :py:mod:`userdbs` app to allow management of user databases
  via :py:mod:`mysqltasks` and :py:mod:`pgsqltasks`
* :feature:`-` add new app :py:mod:`taskresults` that takes care of handling
  asynchronous `Celery`_ results
* :feature:`-` add new task :py:func:`osusers.tasks.delete_ldap_group` (needs
  gvaldap >= 0.2.0 on the LDAP side)
* :feature:`-` add a `customer` field to :py:class:`osusers.models.User`
* :feature:`-` allow empty password input in
  :py:class:`osusers.admin.UserCreationForm` to allow generated passwords for
  new users

* :release:`0.3.0 <2014-12-27>`
* :feature:`-` call create/delete mailbox tasks when saving/deleting mailboxes
* :support:`-` use celery routers from gvacommon
* :feature:`-` automatic creation of mailbox names from user names
* :bug:`- major` fix broken mailbox admin

* :release:`0.2.3 <2014-12-26>`
* :bug:`-` remove attribute readonly_fields from
  :py:class:`osusers.admin.UserAdmin` to make saving of additional groups work

* :release:`0.2.2 <2014-12-26>`
* :feature:`-` home and mail base directory creation

* :release:`0.2.1 <2014-12-17>`
* :support:`-` update Django to 1.7.1, update other dependencies, drop South
* :bug:`-` wrap :py:meth:`ousers.models.UserManager.create_user` in
  transaction.atomic

* :release:`0.2.0 <2014-06-01>`
* :feature:`-` full test suite for osusers
* :feature:`-` full test suite for managemails app
* :feature:`-` full test suite for domains app
* :feature:`-` `Celery`_ integration for ldap synchronization

* :release:`0.1 <2014-05-25>`
* :feature:`-` initial model code for os users
* :feature:`-` initial model code for mail address and mailbox management
* :feature:`-` initial model code for domains

.. _Celery: http://www.celeryproject.org/
