Tests
=====

To run the tests you can just use the :program:`manage.py` script:

.. code-block:: sh

    $ python manage.py test

Coverage
--------

To capture test coverage information you can run:

.. code-block:: sh

    $ coverage run --branch manage.py test

To view the coverage data use:

.. code-block:: sh

    $ coverage report -m

The coverage configuration is in :file:`.coveragerc`. Add new apps to the
`source` configuration in the `[run]` section of that configuration file.
