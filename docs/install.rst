.. index:: installation

=======
Install
=======

Working Environment
===================

To get a running work environment use `pipenv`_.

.. _pipenv: https://pipenv.kennethreitz.org/en/latest/

To get started install `pip` and `pipenv` and use `pipenv install --dev`:

.. code-block:: sh

   $ apt install python3-pip
   $ python3 -m pip install --user -U pipenv
   $ pipenv install --dev

    $ cd gnuviechadmin && add2virtualenv `pwd`
