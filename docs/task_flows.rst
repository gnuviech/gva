**********
Task Flows
**********

gva uses Celery tasks to trigger actions on several servers, this chapter lists
the code parts that start tasks. See the code documentation for details on the
information flow.

:py:mod:`osusers.admin`
=======================

 * :py:meth:`osusers.admin.SshPublicKeyAdmin.perform_delete_selected`


:py:mod:`osusers.signals`
=========================

 * :py:func:`osusers.signals.handle_group_created`
 * :py:func:`osusers.signals.handle_group_deleted`
 * :py:func:`osusers.signals.handle_ssh_keys_changed`
 * :py:func:`osusers.signals.handle_user_added_to_group`
 * :py:func:`osusers.signals.handle_user_created`
 * :py:func:`osusers.signals.handle_user_deleted`
 * :py:func:`osusers.signals.handle_user_password_set`
 * :py:func:`osusers.signals.handle_user_removed_from_group`


:py:mod:`userdbs.signals`
=========================

 * :py:func:`userdbs.signals.handle_dbuser_created`
 * :py:func:`userdbs.signals.handle_dbuser_deleted`
 * :py:func:`userdbs.signals.handle_dbuser_deleted`
 * :py:func:`userdbs.signals.handle_dbuser_password_set`
 * :py:func:`userdbs.signals.handle_userdb_created`
