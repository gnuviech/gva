******************
Code documentation
******************

.. index:: Django

gva is implemented as `Django`_ project and provides a frontend for
administrators and customers.

.. _Django: https://www.djangoproject.com/

Common code
===========

.. toctree::

   code/gvacommon
   code/gvawebcore


Celery task stubs
=================

.. toctree::

   code/fileservertasks
   code/ldaptasks
   code/mysqltasks
   code/pgsqltasks
   code/webtasks


Django app code
===============

.. toctree::

   code/gnuviechadmin
   code/contact_form
   code/dashboard
   code/domains
   code/hostingpackages
   code/managemails
   code/osusers
   code/taskresults
   code/userdbs
   code/websites
