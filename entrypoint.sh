#!/bin/sh

set -e

chown -Rc gva.gva /srv/gva/media /srv/gva/static

su -c /srv/gva.sh gva
