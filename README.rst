gnuviechadmin
-------------

This is the GNUViech Admin Customer Center for gnuviech servers.

GNUViech Admin is a suite of tools for server management used for hosting
customer management at `Jan Dittberner IT-Consulting & -Solutions
<http://www.gnuviech-server.de>`_.

Gnuviechadmin is based on Django_ and Celery_

.. _Django: https://djangoproject.com/
.. _Celery: http://www.celeryproject.com/

The project page for gnuviechadmin is at
http://git.dittberner.info/gnuviech/gva. If you find some problem or have some
feature suggestions you can post a new ticket in our issue tracker on the
project page.
