#!/bin/sh

set -e

DB_HOST="${GVA_PGSQL_HOST:-db}"
DB_PORT="${GVA_PGSQL_PORT:-5432}"
DB_USER="${GVA_PGSQL_USER:-gnuviechadmin}"
DB_NAME="${GVA_PGSQL_DATABASE:-gnuviechadmin}"

until pg_isready -q -h "${DB_HOST}" -p "${DB_PORT}" -U "${DB_USER}" -d "${DB_NAME}"
do
  # shellcheck disable=SC2039
  echo -n "."
  sleep 1
done

echo ". db is ready"

export TZ="Europe/Berlin"

. /srv/gva/.venv/bin/activate
cd /srv/gva/gnuviechadmin
python3 manage.py collectstatic --noinput
python3 manage.py migrate --noinput
python3 manage.py runserver 0.0.0.0:8000
