from django.contrib.auth import get_user_model
from rest_framework import generics

from invoices.models import Invoice
from invoices.serializers import InvoiceSerializer


# Create your views here.
class ListInvoiceAPIView(generics.ListCreateAPIView):
    """
    API endpoint that allows invoice to be viewed or edited.

    """

    queryset = Invoice.objects.all().order_by("-invoice_date", "customer__username")
    serializer_class = InvoiceSerializer


class InvoiceAPIView(generics.RetrieveUpdateAPIView):
    """
    API endpoint for retrieving and updating invoices.

    """

    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    lookup_field = "invoice_number"
    lookup_url_kwarg = "invoice_number"
