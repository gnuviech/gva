from django.contrib.auth import get_user_model
from rest_framework import serializers

from invoices.models import Invoice

User = get_user_model()


class InvoiceSerializer(serializers.ModelSerializer):
    customer = serializers.SlugRelatedField(
        queryset=User.objects.all(), slug_field="username"
    )

    class Meta:
        model = Invoice
        fields = [
            "url",
            "customer",
            "invoice",
            "invoice_number",
            "invoice_date",
            "invoice_value",
            "invoice_currency",
            "due_date",
            "payment_date",
            "payment_variant",
        ]
        extra_kwargs = {
            "url": {
                "lookup_field": "invoice_number",
                "lookup_url_kwarg": "invoice_number",
            },
        }
