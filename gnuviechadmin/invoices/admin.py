from django.contrib import admin

from invoices.models import Invoice


class InvoiceAdmin(admin.ModelAdmin):
    list_display = ["invoice_number", "customer"]
    readonly_fields = ["customer"]

    def has_add_permission(self, request):
        return False


admin.site.register(Invoice, InvoiceAdmin)
