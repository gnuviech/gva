# import celery_app to initialize it
from gnuviechadmin.celery import app as celery_app  # NOQA

__version__ = "0.15.1"
