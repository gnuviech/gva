# -*- python -*-
# -*- coding: utf-8 -*-
from unittest import TestCase


class WSGITest(TestCase):

    def test_wsgi_application(self):
        from gnuviechadmin import wsgi
        self.assertIsNotNone(wsgi.application)
