from unittest import TestCase

from gnuviechadmin.celery import get_installed_apps
from django.conf import settings


class GetInstalledAppsTest(TestCase):

    def test_get_installed_apps(self):
        self.assertEqual(get_installed_apps(), settings.INSTALLED_APPS)
