# -*- python -*-
# -*- coding: utf-8 -*-
"""
This module contains tests for :py:mod:`gnuviechadmin.context_processors`.

"""

from unittest.mock import MagicMock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.test import TestCase
from django.urls import reverse

from gnuviechadmin import __version__ as gvaversion
from gnuviechadmin.context_processors import navigation

User = get_user_model()

TEST_USER = "test"
TEST_PASSWORD = "secret"


class NavigationContextProcessorTest(TestCase):

    EXPECTED_ITEMS = ("webmail_url", "phpmyadmin_url", "phppgadmin_url", "active_item")

    def test_ajax_request(self):
        response = self.client.get("/accounts/login/", HTTP_X_REQUESTED_WITH="XMLHttpRequest")
        for item in self.EXPECTED_ITEMS:
            self.assertNotIn(item, response.context)

    def _check_static_urls(self, context):
        self.assertEqual(context["webmail_url"], settings.GVA_LINK_WEBMAIL)
        self.assertEqual(context["phpmyadmin_url"], settings.GVA_LINK_PHPMYADMIN)
        self.assertEqual(context["phppgadmin_url"], settings.GVA_LINK_PHPPGADMIN)

    def test_index_page_context(self):
        user = User.objects.create(username=TEST_USER)
        user.set_password(TEST_PASSWORD)
        user.save()

        self.client.login(username=TEST_USER, password=TEST_PASSWORD)

        response = self.client.get("/")
        for item in self.EXPECTED_ITEMS:
            self.assertIn(item, response.context)
        self._check_static_urls(response.context)
        self.assertEqual(response.context["active_item"], "dashboard")

    def test_contact_page_context(self):
        response = self.client.get(reverse("contact_form"))
        for item in self.EXPECTED_ITEMS:
            self.assertIn(item, response.context)
        self._check_static_urls(response.context)
        self.assertEqual(response.context["active_item"], "contact")

    def _test_page_context_by_viewmodule(self, viewmodule, expecteditem):
        request = HttpRequest()
        request.resolver_match = MagicMock()
        request.resolver_match.func.__module__ = viewmodule
        context = navigation(request)
        for item in self.EXPECTED_ITEMS:
            self.assertIn(item, context)
        self._check_static_urls(context)
        self.assertEqual(context["active_item"], expecteditem)

    def test_osusers_page_context(self):
        self._test_page_context_by_viewmodule("osusers.views", "hostingpackage")

    def test_userdbs_page_context(self):
        self._test_page_context_by_viewmodule("userdbs.views", "hostingpackage")

    def test_managemails_page_context(self):
        self._test_page_context_by_viewmodule("managemails.views", "hostingpackage")

    def test_websites_page_context(self):
        self._test_page_context_by_viewmodule("websites.views", "hostingpackage")

    def test_domains_page_context(self):
        self._test_page_context_by_viewmodule("domains.views", "hostingpackage")

    def test_allauth_account_page_context(self):
        self._test_page_context_by_viewmodule("allauth.account.views", "account")

    def test_allauth_socialaccount_page_context(self):
        self._test_page_context_by_viewmodule("allauth.socialaccount.views", "account")

    def test_imprint_page_context(self):
        response = self.client.get(reverse("imprint"))
        for item in self.EXPECTED_ITEMS:
            self.assertIn(item, response.context)
        self._check_static_urls(response.context)
        self.assertEqual(response.context["active_item"], "imprint")

    def test_no_resolver_match(self):
        request = HttpRequest()
        context = navigation(request)
        self._check_static_urls(context)
        self.assertEqual(context["active_item"], "dashboard")

    def test_admin_module(self):
        self._test_page_context_by_viewmodule("django.contrib.admin.foo", "dashboard")


class VersionInfoContextProcessorTest(TestCase):
    def test_version_info_in_context(self):
        response = self.client.get("/accounts/login/")
        self.assertIn("gnuviechadmin_version", response.context)
        self.assertEqual(response.context["gnuviechadmin_version"], gvaversion)
