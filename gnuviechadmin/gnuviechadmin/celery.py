from __future__ import absolute_import

import os

from celery import Celery

from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gnuviechadmin.settings")


app = Celery("gnuviechadmin")


def get_installed_apps():
    return settings.INSTALLED_APPS


app.config_from_object("django.conf:settings")
app.autodiscover_tasks(get_installed_apps)
