from __future__ import absolute_import

import debug_toolbar
from django.conf.urls import include
from django.contrib import admin
from django.contrib.flatpages import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from help import views as help_views
from invoices import views as invoice_views

admin.autodiscover()

urlpatterns = [
    path("", include("dashboard.urls")),
    path("api/users/", help_views.ListHelpUserAPIView.as_view()),
    path(
        "api/users/<int:pk>/",
        help_views.HelpUserAPIView.as_view(),
        name="helpuser-detail",
    ),
    path("api/invoices/", invoice_views.ListInvoiceAPIView.as_view()),
    path(
        "api/invoices/<invoice_number>/",
        invoice_views.InvoiceAPIView.as_view(),
        name="invoice-detail",
    ),
    path("admin/", admin.site.urls),
    path("impersonate/", include("impersonate.urls")),
    path("accounts/", include("allauth.urls")),
    path("database/", include("userdbs.urls")),
    path("domains/", include("domains.urls")),
    path("hosting/", include("hostingpackages.urls")),
    path("website/", include("websites.urls")),
    path("mail/", include("managemails.urls")),
    path("osuser/", include("osusers.urls")),
    path("contact/", include("contact_form.urls")),
    path("impressum/", views.flatpage, {"url": "/impressum/"}, name="imprint"),
    path("datenschutz/", views.flatpage, {"url": "/datenschutz/"}, name="privacy"),
    path("issues/", views.flatpage, {"url": "/issues/"}, name="support"),
]

# Uncomment the next line to serve media files in dev.
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += [
    path("__debug__/", include(debug_toolbar.urls)),
]
