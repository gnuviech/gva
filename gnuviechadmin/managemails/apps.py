"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`managemails` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ManageMailsAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`managemails` app.

    """

    name = "managemails"
    verbose_name = _("Mailboxes and Mail Addresses")

    def ready(self):
        """
        Takes care of importing the signal handlers of the :py:mod:`managemails`
        app.

        """
        import managemails.signals  # NOQA
