"""
This module defines the URL patterns for mailbox and mail address related
views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import (
    AddMailAddress,
    ChangeMailboxPassword,
    CreateMailbox,
    DeleteMailAddress,
    EditMailAddress,
)

urlpatterns = [
    re_path(
        r"^(?P<package>\d+)/mailbox/create$",
        CreateMailbox.as_view(),
        name="create_mailbox",
    ),
    re_path(
        r"^(?P<package>\d+)/mailbox/(?P<slug>[\w0-9]+)/setpassword$",
        ChangeMailboxPassword.as_view(),
        name="change_mailbox_password",
    ),
    re_path(
        r"^(?P<package>\d+)/mailaddress/(?P<domain>[\w0-9-.]+)/create$",
        AddMailAddress.as_view(),
        name="add_mailaddress",
    ),
    re_path(
        r"^(?P<package>\d+)/mailaddress/(?P<domain>[\w0-9-.]+)/(?P<pk>\d+)" r"/edit$",
        EditMailAddress.as_view(),
        name="edit_mailaddress",
    ),
    re_path(
        r"^(?P<package>\d+)/mailaddress/(?P<domain>[\w0-9-.]+)/(?P<pk>\d+)" r"/delete$",
        DeleteMailAddress.as_view(),
        name="delete_mailaddress",
    ),
]
