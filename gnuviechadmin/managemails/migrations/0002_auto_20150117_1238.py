# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("managemails", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="mailaddress",
            options={
                "ordering": ["domain", "localpart"],
                "verbose_name": "Mail address",
                "verbose_name_plural": "Mail addresses",
            },
        ),
        migrations.AlterModelOptions(
            name="mailbox",
            options={
                "ordering": ["osuser", "username"],
                "verbose_name": "Mailbox",
                "verbose_name_plural": "Mailboxes",
            },
        ),
    ]
