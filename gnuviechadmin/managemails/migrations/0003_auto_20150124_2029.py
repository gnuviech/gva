# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("managemails", "0002_auto_20150117_1238"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mailaddressmailbox",
            name="mailaddress",
            field=models.OneToOneField(
                primary_key=True,
                serialize=False,
                to="managemails.MailAddress",
                verbose_name="mailaddress",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="mailaddressmailbox",
            name="mailbox",
            field=models.ForeignKey(
                verbose_name="mailbox",
                to="managemails.Mailbox",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
