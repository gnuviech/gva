# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("managemails", "0003_auto_20150124_2029"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mailaddress",
            name="domain",
            field=models.ForeignKey(
                verbose_name="domain", to="domains.MailDomain", on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="mailaddress",
            name="localpart",
            field=models.CharField(max_length=128, verbose_name="local part"),
            preserve_default=True,
        ),
    ]
