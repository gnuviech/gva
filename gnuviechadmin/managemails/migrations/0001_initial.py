# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("domains", "0001_initial"),
        ("osusers", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="MailAddress",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                ("active", models.BooleanField(default=True)),
                ("localpart", models.CharField(max_length=128)),
            ],
            options={
                "verbose_name": "Mail address",
                "verbose_name_plural": "Mail addresses",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="MailAddressForward",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                ("target", models.EmailField(max_length=254)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="MailAddressMailbox",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "mailaddress",
                    models.OneToOneField(
                        primary_key=True,
                        serialize=False,
                        to="managemails.MailAddress",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="Mailbox",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                ("active", models.BooleanField(default=True)),
                ("username", models.CharField(unique=True, max_length=128)),
                ("password", models.CharField(max_length=255)),
                (
                    "osuser",
                    models.ForeignKey(to="osusers.User", on_delete=models.CASCADE),
                ),
            ],
            options={
                "verbose_name": "Mailbox",
                "verbose_name_plural": "Mailboxes",
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name="mailaddressmailbox",
            name="mailbox",
            field=models.ForeignKey(to="managemails.Mailbox", on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="mailaddressmailbox",
            unique_together={("mailaddress", "mailbox")},
        ),
        migrations.AddField(
            model_name="mailaddressforward",
            name="mailaddress",
            field=models.ForeignKey(
                to="managemails.MailAddress", on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="mailaddressforward",
            unique_together={("mailaddress", "target")},
        ),
        migrations.AddField(
            model_name="mailaddress",
            name="domain",
            field=models.ForeignKey(to="domains.MailDomain", on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="mailaddress",
            unique_together={("localpart", "domain")},
        ),
    ]
