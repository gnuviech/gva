"""
Tests for :py:mod:`managemails.views`.

"""
from unittest.mock import patch, MagicMock

from django.test import TestCase
from django.test.utils import override_settings
from django.contrib.auth import get_user_model
from django.urls import reverse

from domains.models import MailDomain
from hostingpackages.models import CustomerHostingPackage, HostingPackageTemplate

from managemails.forms import MAILBOX_OR_FORWARDS
from managemails.models import MailAddress, Mailbox
from managemails.views import (
    AddMailAddress,
    ChangeMailboxPassword,
    CreateMailbox,
    EditMailAddress,
)


User = get_user_model()

TEST_USER = "test"
TEST_PASSWORD = "secret"
TEST_EMAIL = "test@example.org"
TEST_NAME = "Example Tester".split()


class HostingPackageAwareTestMixin(object):

    # noinspection PyMethodMayBeStatic
    def _setup_hosting_package(self, customer, mailboxcount):
        template = HostingPackageTemplate.objects.create(
            name="testpackagetemplate",
            mailboxcount=mailboxcount,
            diskspace=1,
            diskspace_unit=0,
        )
        package = CustomerHostingPackage.objects.create_from_template(
            customer, template, "testpackage"
        )
        with patch("hostingpackages.models.settings") as hmsettings:
            hmsettings.OSUSER_DEFAULT_GROUPS = []
            package.save()
        return package


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class CreateMailboxTest(HostingPackageAwareTestMixin, TestCase):
    def test_get_anonymous(self):
        response = self.client.get(reverse("create_mailbox", kwargs={"package": 1}))
        self.assertEqual(response.status_code, 404)

    def test_get_regular_user(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_get_other_regular_user(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        User.objects.create_user(
            "test2", email="test2@example.org", password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username="test2", password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer, 1)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertTemplateUsed(response, "managemails/mailbox_create.html")

    def test_get_no_more_mailboxes(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 0)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.content,
            b"You are not allowed to add more mailboxes to this hosting" b" package",
        )

    def test_get_context_data(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_mailbox", kwargs={"package": package.pk})
        )
        self.assertIn("hostingpackage", response.context)
        self.assertEqual(response.context["hostingpackage"], package)
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], customer)

    def test_get_form_kwargs(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = CreateMailbox(request=MagicMock(), kwargs={"package": str(package.pk)})
        the_kwargs = view.get_form_kwargs()
        self.assertIn("hostingpackage", the_kwargs)
        self.assertEqual(the_kwargs["hostingpackage"], package)

    def test_form_valid_redirect(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse("create_mailbox", kwargs={"package": package.pk}),
            data={"password1": TEST_PASSWORD, "password2": TEST_PASSWORD},
        )
        self.assertRedirects(response, package.get_absolute_url())

    def test_form_valid_message(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer, 1)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse("create_mailbox", kwargs={"package": package.pk}),
            follow=True,
            data={"password1": TEST_PASSWORD, "password2": TEST_PASSWORD},
        )
        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual("Mailbox usr01p01 created successfully.", str(messages[0]))


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class ChangeMailboxPasswordTest(HostingPackageAwareTestMixin, TestCase):
    def setUp(self):
        self.customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.package = self._setup_hosting_package(self.customer, 1)
        self.mailbox = Mailbox.objects.create_mailbox(
            self.package.osuser, TEST_PASSWORD
        )

    def test_get_anonymous(self):
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_regular_user(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_other_regular_user(self):
        User.objects.create_user(
            "test2", email="test2@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="test2", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        User.objects.create_superuser(
            "admin", email="admin@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="admin", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertTemplateUsed(response, "managemails/mailbox_setpassword.html")

    def test_get_context_data(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            )
        )
        self.assertIn("hostingpackage", response.context)
        self.assertEqual(response.context["hostingpackage"], self.package)
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], self.customer)

    def test_get_form_kwargs(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = ChangeMailboxPassword(
            request=MagicMock(),
            kwargs={"package": str(self.package.pk), "slug": self.mailbox.username},
        )
        the_kwargs = view.get_form_kwargs()
        self.assertIn("hostingpackage", the_kwargs)
        self.assertEqual(the_kwargs["hostingpackage"], self.package)

    def test_form_valid_redirect(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            ),
            data={"password1": TEST_PASSWORD, "password2": TEST_PASSWORD},
        )
        self.assertRedirects(response, self.package.get_absolute_url())

    def test_form_valid_message(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "change_mailbox_password",
                kwargs={"package": self.package.pk, "slug": self.mailbox.username},
            ),
            follow=True,
            data={"password1": TEST_PASSWORD, "password2": TEST_PASSWORD},
        )
        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), "Successfully set new password for mailbox usr01p01."
        )


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class AddMailAddressTest(HostingPackageAwareTestMixin, TestCase):
    def setUp(self):
        self.customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.package = self._setup_hosting_package(self.customer, 1)
        self.maildomain = MailDomain.objects.create(
            domain="example.org", customer=self.customer
        )

    def test_get_anonymous(self):
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_regular_user(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_other_regular_user(self):
        User.objects.create_user(
            "test2", email="test2@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="test2", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        User.objects.create_superuser(
            "admin", email="admin@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="admin", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertTemplateUsed(response, "managemails/mailaddress_create.html")

    def test_get_context_data(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            )
        )
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], self.customer)

    def test_get_form_kwargs(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = AddMailAddress(
            request=MagicMock(),
            kwargs={"package": str(self.package.pk), "domain": self.maildomain.domain},
        )
        the_kwargs = view.get_form_kwargs()
        self.assertIn("hostingpackage", the_kwargs)
        self.assertEqual(the_kwargs["hostingpackage"], self.package)
        self.assertIn("maildomain", the_kwargs)
        self.assertEqual(the_kwargs["maildomain"], self.maildomain)

    def test_form_valid_redirect(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            ),
            data={
                "localpart": "test",
                "mailbox_or_forwards": MAILBOX_OR_FORWARDS.forwards,
                "mailbox": "",
                "forwards": "test2@example.org",
            },
        )
        self.assertRedirects(response, self.package.get_absolute_url())

    def test_form_valid_message(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "add_mailaddress",
                kwargs={"package": self.package.pk, "domain": self.maildomain.domain},
            ),
            follow=True,
            data={
                "localpart": "test",
                "mailbox_or_forwards": MAILBOX_OR_FORWARDS.forwards,
                "mailbox": "",
                "forwards": "test2@example.org",
            },
        )
        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), "Successfully added mail address test@example.org"
        )


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class DeleteMailAddressTest(HostingPackageAwareTestMixin, TestCase):
    def setUp(self):
        self.customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.package = self._setup_hosting_package(self.customer, 1)
        self.maildomain = MailDomain.objects.create(
            domain="example.org", customer=self.customer
        )
        self.mailaddress = MailAddress.objects.create(
            localpart="test", domain=self.maildomain
        )
        self.mailaddress.set_forward_addresses(["test2@example.org"])

    def test_get_anonymous(self):
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_regular_user(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_other_regular_user(self):
        User.objects.create_user(
            "test2", email="test2@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="test2", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        User.objects.create_superuser(
            "admin", email="admin@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="admin", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertTemplateUsed(response, "managemails/mailaddress_confirm_delete.html")

    def test_get_context_data(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], self.customer)
        self.assertIn("hostingpackage", response.context)
        self.assertEqual(response.context["hostingpackage"], self.package)
        self.assertIn("maildomain", response.context)
        self.assertEqual(response.context["maildomain"], self.maildomain)

    def test_form_valid_redirect(self):
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "delete_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            ),
            data={},
        )
        self.assertRedirects(response, self.package.get_absolute_url())


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class EditMailAddressTest(HostingPackageAwareTestMixin, TestCase):
    def setUp(self):
        self.customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.package = self._setup_hosting_package(self.customer, 1)
        self.maildomain = MailDomain.objects.create(
            domain="example.org", customer=self.customer
        )

    def _set_mailaddress_with_forward(self):
        self.mailaddress = MailAddress.objects.create(
            localpart="test", domain=self.maildomain
        )
        self.mailaddress.set_forward_addresses(["test2@example.org"])

    def _set_mailaddress_with_mailbox(self):
        self.mailaddress = MailAddress.objects.create(
            localpart="test", domain=self.maildomain
        )
        self.mailbox = Mailbox.objects.create_mailbox(self.package.osuser)
        self.mailaddress.set_mailbox(self.mailbox)

    def test_get_anonymous(self):
        self._set_mailaddress_with_forward()
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_regular_user(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_other_regular_user(self):
        self._set_mailaddress_with_forward()
        User.objects.create_user(
            "test2", email="test2@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="test2", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        self._set_mailaddress_with_forward()
        User.objects.create_superuser(
            "admin", email="admin@example.org", password=TEST_PASSWORD
        )
        self.client.login(username="admin", password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertTemplateUsed(response, "managemails/mailaddress_edit.html")

    def test_get_context_data(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            )
        )
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], self.customer)

    def test_get_form_kwargs(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = EditMailAddress(
            request=MagicMock(),
            kwargs={
                "package": str(self.package.pk),
                "domain": self.maildomain.domain,
                "pk": str(self.mailaddress.pk),
            },
        )
        the_kwargs = view.get_form_kwargs()
        self.assertIn("hostingpackage", the_kwargs)
        self.assertEqual(the_kwargs["hostingpackage"], self.package)
        self.assertIn("maildomain", the_kwargs)
        self.assertEqual(the_kwargs["maildomain"], self.maildomain)

    def test_get_initial_with_forwards(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = EditMailAddress(
            request=MagicMock(),
            kwargs={
                "package": str(self.package.pk),
                "domain": self.maildomain.domain,
                "pk": str(self.mailaddress.pk),
            },
        )
        initial = view.get_initial()
        self.assertIn("mailbox_or_forwards", initial)
        self.assertEqual(initial["mailbox_or_forwards"], MAILBOX_OR_FORWARDS.forwards)
        self.assertIn("forwards", initial)
        self.assertEqual(initial["forwards"], "test2@example.org")
        self.assertNotIn("mailbox", initial)

    def test_get_initial_with_mailbox(self):
        self._set_mailaddress_with_mailbox()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = EditMailAddress(
            request=MagicMock(),
            kwargs={
                "package": str(self.package.pk),
                "domain": self.maildomain.domain,
                "pk": str(self.mailaddress.pk),
            },
        )
        initial = view.get_initial()
        self.assertIn("mailbox_or_forwards", initial)
        self.assertEqual(initial["mailbox_or_forwards"], MAILBOX_OR_FORWARDS.mailbox)
        self.assertIn("mailbox", initial)
        self.assertEqual(initial["mailbox"], self.mailbox)
        self.assertNotIn("forwards", initial)

    def test_get_initial_with_unassigned_address(self):
        self.mailaddress = MailAddress.objects.create(
            localpart="test", domain=self.maildomain
        )
        view = EditMailAddress(
            request=MagicMock(),
            kwargs={
                "package": str(self.package.pk),
                "domain": self.maildomain.domain,
                "pk": str(self.mailaddress.pk),
            },
        )
        initial = view.get_initial()
        self.assertNotIn("mailbox_or_forwards", initial)
        self.assertNotIn("forwards", initial)
        self.assertNotIn("mailbox", initial)

    def test_form_valid_redirect(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            ),
            data={
                "mailbox_or_forwards": MAILBOX_OR_FORWARDS.forwards,
                "mailbox": "",
                "forwards": "test2@example.org,test3@example.org",
            },
        )
        self.assertRedirects(response, self.package.get_absolute_url())

    def test_form_valid_message(self):
        self._set_mailaddress_with_forward()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse(
                "edit_mailaddress",
                kwargs={
                    "package": self.package.pk,
                    "domain": self.maildomain.domain,
                    "pk": self.mailaddress.pk,
                },
            ),
            follow=True,
            data={
                "mailbox_or_forwards": MAILBOX_OR_FORWARDS.forwards,
                "mailbox": "",
                "forwards": "test2@example.org,test3@example.org",
            },
        )
        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]),
            "Successfully updated mail address test@example.org targets.",
        )
