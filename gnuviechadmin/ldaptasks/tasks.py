"""
This module defines task stubs for the tasks implemented in the gvaldap Celery
worker.

"""
from __future__ import absolute_import

from celery import shared_task


@shared_task
def create_ldap_group(groupname, gid, description):
    """
    This task creates an :py:class:`LDAP group <ldapentities.models.LdapGroup>`
    if it does not exist yet.

    If a group with the given name exists its group id and description
    attributes are updated.

    :param str groupname: the group name
    :param int gid: the group id
    :param str description: description text for the group
    :return: dictionary containing groupname, gid, description and
        :py:const:`group_dn` set to the distinguished name of the newly created
        or existing LDAP group
    :rtype: dict

    """


@shared_task
def create_ldap_user(username, uid, gid, gecos, homedir, shell, password):
    """
    This task creates an :py:class:`LDAP user <ldapentities.models.LdapUser>`
    if it does not exist yet.

    The task is rejected if the primary group of the user is not defined.

    The user's fields are updated if the user already exists.

    :param str username: the user name
    :param int uid: the user id
    :param int gid: the user's primary group's id
    :param str gecos: the text for the GECOS field
    :param str homedir: the user's home directory
    :param str shell: the user's login shell
    :param str or None password: the clear text password, if :py:const:`None`
        is passed the password is not touched
    :raises celery.exceptions.Reject: if the specified primary group does not
        exist
    :return: dictionary containing username, uid, gid, gecos, homedir, shell,
        password and :py:const:`user_dn` set to the distinguished name of the
        newly created or existing LDAP user
    :rtype: dict

    """


@shared_task
def set_ldap_user_password(username, password):
    """
    This task sets the password of an existing :py:class:`LDAP user
    <ldapentities.models.LdapUser>`.

    :param str username: the user name
    :param str password: teh clear text password
    :return: dictionary containing the username and a flag
        :py:const:`password_set` that is set to  :py:const:`True` if the
        password has been set, :py:const:`False` if the user does not exist.
    :rtype: dict

    """


@shared_task
def add_ldap_user_to_group(username, groupname):
    """
    This task adds the specified user to the given group.

    This task does nothing if the user is already member of the group.

    :param str username: the user name
    :param str groupname: the group name
    :raises celery.exceptions.Retry: if the user does not exist yet,
        :py:func:`create_ldap_user` should be called before
    :return: dictionary containing the username, groupname and a flag
        :py:const`added` that is as a :py:const:`True` if the user has been
        added to the group otherwise to :py:const:`False`
    :rtype: dict

    """


@shared_task
def remove_ldap_user_from_group(username, groupname):
    """
    This task removes the given user from the given group.

    :param str username: the user name
    :param str groupname: the group name
    :return: dictionary containing the input parameters and a flag
        :py:const:`removed` that is set to :py:const:`True` if the user has
        been removed, False otherwise
    :rtype: dict

    """


@shared_task
def delete_ldap_user(username, *args, **kwargs):
    """
    This task deletes the given user.

    :param str username: the user name
    :return: dictionary containing the username and a flag :py:const:`deleted`
        that is set to :py:const:`True` if the user has been deleted and is set
        to :py:const:`False` otherwise
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`ldaptasks.tasks.delete_ldap_user_chained` at other
        positions in the task chain

    """


@shared_task
def delete_ldap_user_chained(previous_result, *args, **kwargs):
    """
    This task deletes the given user.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` key
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`deleted` key set to :py:const:`True` if the user has been
        deleted and set to :py:const:`False` otherwise
    :rtype: dict

    """


@shared_task
def delete_ldap_group_if_empty(groupname):
    """
    This task deletes the given group if it is empty.

    :param str groupname: the group name
    :return: dictionary that contains the groupname and a flag
        :py:const:`deleted` that is set to :py:const:`True` if the group has
        been deleted and is set to :py:const:`False` otherwise
    :rtype: dict

    """


@shared_task
def delete_ldap_group(groupname):
    """
    This task deletes the given group.

    :param str groupname: the group name
    :return: dictionary that contains the groupname and a flag
        :py:const:`deleted` that is set to :py:const:`True` if the group has
        been deleted and is set to :py:const:`False` otherwise
    :rtype: dict

    """
