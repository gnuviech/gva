"""
This module defines task stubs for the tasks implemented in the gvafile Celery
worker.

"""
from __future__ import absolute_import

from celery import shared_task


@shared_task
def setup_file_sftp_userdir(username, *args, **kwargs):
    """
    This task creates the home directory for an SFTP user if it does not exist
    yet.

    :param str username: the username
    :raises Exception: if the SFTP directory of the user cannot be created
    :return: a dictionary with the key :py:const:`username` set to the
        username value and a new key :py:const:`sftp_directory` set to the
        path of the created SFTP directory
    :rtype: dict

    .. note::

       This variant can only be used at the beginning of a Celery task chain
       or as a standalone task.

       Use :py:func:`fileservertasks.tasks.setup_file_sftp_userdir_chained`
       at other positions in the task chain.

    """


@shared_task
def setup_file_sftp_userdir_chained(previous_result, *args, **kwargs):
    """
    This task creates the home directory for an SFTP user if it does not exist
    yet.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` key
    :raises Exception: if the SFTP directory of the user cannot be created
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`sftp_directory` key set to the path of the created SFTP
        directory
    :rtype: dict

    """


@shared_task
def delete_file_sftp_userdir(username, *args, **kwargs):
    """
    This task recursively deletes the home directory of an SFTP user if it
    exists.

    :param str username: the username
    :raises Exception: if the SFTP directory of the user cannot be removed
    :return: a dictionary with the key :py:const:`username` set to the username
        value and the new key :py:const:`sftp_directory` set to the path of the
        deleted SFTP directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`fileservertasks.tasks.delete_file_sftp_userdir_chained`
        at other positions in the task chain.

    """


@shared_task
def delete_file_sftp_userdir_chained(previous_result, *args, **kwargs):
    """
    This task recursively deletes the home directory of an SFTP user if it
    exists.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` key
    :raises Exception: if the SFTP directory of the user cannot be removed
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`sftp_directory` key set to the path of the removed SFTP
        directory
    :rtype: dict

    """


@shared_task
def setup_file_mail_userdir(username, *args, **kwargs):
    """
    This task creates the mail base directory for a user if it does not exist
    yet.

    :param str username: the username
    :raises Exception: if the mail base directory for the user cannot be
        created
    :return: a dictionary with the key :py:const:`username` set to the
        username value and a new key :py:const:`mail_directory` set to the path
        of the created mail directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`fileservertasks.tasks.setup_file_mail_userdir_chained`
        at other positions in the task chain.

    """


@shared_task
def setup_file_mail_userdir_chained(previous_result, *args, **kwargs):
    """
    This task creates the mail base directory for a user if it does not exist
    yet.

    :param dict previous_result: a dictionary containing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` key
    :raises Exception: if the mail base directory for the user cannot be
        created
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`mail_directory` key set to the path of the created mail
        directory
    :rtype: dict

    """


@shared_task
def delete_file_mail_userdir(username, *args, **kwargs):
    """
    This task recursively deletes the mail base directory for a user if it
    does not exist yet.

    :param str username: the username
    :raises Exception: if the mail base directory of the user cannot be deleted
    :return: a dictionary with the key :py:const:`username` set to the
        username value and a new key :py:const:`mail_directory` set to the path
        of the deleted mail directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`fileservertasks.tasks.delete_file_mail_userdir_chained`
        at other positions in the task chain.

    """


@shared_task
def delete_file_mail_userdir_chained(previous_result, *args, **kwargs):
    """
    This task recursively deletes the mail base directory for a user if it
    does not exist yet.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` key
    :raises Exception: if the mail base directory of the user cannot be deleted
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`mail_directory` key set to the path of the deleted mail
        directory
    :rtype: str

    """


@shared_task
def create_file_mailbox(username, mailboxname, *args, **kwargs):
    """
    This task creates a new mailbox directory for the given user and mailbox
    name.

    :param str username: the username
    :param str mailboxname: the mailbox name
    :raises Exception: if the mailbox directory cannot be created
    :return: a dictionary with the keys :py:const:`username` and
        :py:const:`mailboxname` set to the values of username and mailboxname
        and a new key :py:const:`mailbox_directory` set to the path of the
        created mailbox directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`fileservertasks.tasks.create_file_mailbox_chained` at
        other positions in the task chain.

    """


@shared_task
def create_file_mailbox_chained(previous_result, *args, **kwargs):
    """
    This task creates a new mailbox directory for the given user and mailbox
    name.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` and a :py:const:`mailboxname` key
    :raises Exception: if the mailbox directory cannot be created
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`mailbox_directory` key set to the path of the created
        mailbox directory
    :rtype: dict

    """


@shared_task
def delete_file_mailbox(username, mailboxname, *args, **kwargs):
    """
    This task deletes the given mailbox of the given user.

    :param str username: the username
    :param str mailboxname: the mailbox name
    :raises Exception: if the mailbox directory cannot be deleted
    :return: a dictionary with the keys :py:const:`username` and
        :py:const:`mailboxname` set to the values of username and mailboxname
        and a new key :py:const:`mailbox_directory` set to the path of the
        deleted mailbox directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use :py:func:`fileservertasks.tasks.delete_file_mailbox_chained` for
        other positions in the task chain.

    """


@shared_task
def delete_file_mailbox_chained(previous_result, *args, **kwargs):
    """
    This task deletes the given mailbox of the given user.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` and a :py:const:`mailboxname` key
    :raises Exception: if the mailbox directory cannot be deleted
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`mailbox_directory` key set to the path of the deleted
        mailbox directory
    :rtype: dict

    """


@shared_task
def create_file_website_hierarchy(username, sitename, *args, **kwargs):
    """
    This task creates the directory hierarchy for a website.

    :param str username: the username
    :param str sitename: the sitename
    :raises Exception: if the website directory hierarchy directory cannot be
        created
    :return: a dictionary with the keys :py:const:`username` and
        :py:const:`sitename` set to the values of username and sitename and a
        new key :py:const:`website_directory` set to the path of the created
        website directory
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use
        :py:func:`fileservertasks.tasks.create_file_website_hierarchy_chained`
        at other positions in the task chain

    """


@shared_task
def create_file_website_hierarchy_chained(previous_result, *args, **kwargs):
    """
    This task creates the directory hierarchy for a website.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` and a :py:const:`sitename` key
    :raises Exception: if the website directory hierarchy directory cannot be
        created
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`website_directory` key set to the path of the created
        website directory
    :rtype: dict

    """


@shared_task
def delete_file_website_hierarchy(username, sitename, *args, **kwargs):
    """
    This task deletes a website hierarchy recursively.

    :param str username: a username
    :param str sitename: a site name
    :return: a dictionary with the keys :py:const:`username` and
        :py:const:`sitename` set to their original values and a new key
        :py:const:`website_directory` set to the path of the deleted website
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use
        :py:func:`fileservertasks.tasks.delete_file_website_hierarchy_chained`
        at other positions in the task chain

    """


@shared_task
def delete_file_website_hierarchy_chained(previous_result, *args, **kwargs):
    """
    This task deletes the website hierarchy recursively.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` and a :py:const:`sitename` key
    :raises Exception: if the website directory hierarchy directory cannot be
        deleted
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`website_directory` set to the path of the deleted website
        directory
    :rtype: dict

    """


@shared_task
def set_file_ssh_authorized_keys(username, ssh_keys, *args, **kwargs):
    """
    This task set the authorized keys for ssh logins.

    :param str username: a username
    :param list ssh_keys: a list of ssh keys
    :raises Exception: if the update of the creation or update of ssh
        authorized_keys failed
    :return: a dictionary with the keys :py:const:`username` and
        :py:const:`ssh_keys` set to their original values and a new key
        :py:const:`ssh_authorized_keys` set to the path of the SSH
        authorized_keys file
    :rtype: dict

    .. note::

        This variant can only be used at the beginning of a Celery task chain
        or as a standalone task.

        Use
        :py:func:`fileservertasks.tasks.set_file_ssh_authorized_keys_chained`
        at other positions in the task chain

    """


@shared_task
def set_file_ssh_authorized_keys_chained(previous_result, *args, **kwargs):
    """
    This task sets the authorized keys for ssh logins.

    :param dict previous_result: a dictionary describing the result of the
        previous step in the Celery task chain. This dictionary must contain a
        :py:const:`username` and a :py:const:`ssh_keys` key
    :raises Exception: if the update of the creation or update of ssh
        authorized_keys failed
    :return: a copy of the :py:obj:`previous_result` dictionary with a new
        :py:const:`ssh_authorized_keys` set to the path of the SSH
        authorized_keys file
    :rtype: dict

    """
