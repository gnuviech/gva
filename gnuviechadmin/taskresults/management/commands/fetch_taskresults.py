"""
This model contains the implementation of a management command to fetch the
results of all `Celery <http://www.celeryproject.org/>`_ tasks that are not
marked as finished yet.

"""
from django.core.management.base import BaseCommand

from taskresults.models import TaskResult


class Command(BaseCommand):
    help = "fetch task results"

    def handle(self, *args, **options):
        for task_result in TaskResult.objects.filter(finished=False):
            if options["verbosity"] > 1:
                print(f"fetching result for unfinished task {task_result.task_id}", file=self.stderr)
            task_result.fetch_result()
            if options["verbosity"] > 2:
                print(f"fetched result for unfinished task {task_result.task_id}", file=self.stderr)
            task_result.save()
