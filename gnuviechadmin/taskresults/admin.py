"""
This module defines the admin interface for the taskresults app.

"""
from __future__ import absolute_import

from django.contrib import admin

from .models import TaskResult


@admin.register(TaskResult)
class TaskResultAdmin(admin.ModelAdmin):
    list_display = ("task_id", "creator", "signature", "finished", "state")
    list_filter = ("finished", "creator")
    date_hierarchy = "created"
    readonly_fields = ("created", "modified")

    def has_add_permission(self, request):
        return False
