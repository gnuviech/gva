from django.test import TestCase
from django.test.utils import override_settings

from taskresults.models import TaskResult


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class TestCaseWithCeleryTasks(TestCase):
    def resetCeleryTasks(self):
        TaskResult.objects.all().delete()

    def assertCeleryTasksRun(self, tasks):
        task_results = TaskResult.objects.all()

        self.assertEqual(task_results.count(), sum([t[0] for t in tasks]))

        creators = [r.creator for r in task_results]
        for t_count, t_creator in tasks:
            self.assertEqual(creators.count(t_creator), t_count)
