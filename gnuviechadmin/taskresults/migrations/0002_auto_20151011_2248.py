# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("taskresults", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="taskresult",
            name="task_name",
        ),
        migrations.AddField(
            model_name="taskresult",
            name="creator",
            field=models.TextField(default="migrated", verbose_name="Task creator"),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="taskresult",
            name="notes",
            field=models.TextField(default="", verbose_name="Task notes"),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="taskresult",
            name="signature",
            field=models.TextField(default="", verbose_name="Task signature"),
            preserve_default=False,
        ),
    ]
