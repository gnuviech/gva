# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-09 14:24
import django.utils.timezone
import model_utils.fields
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("taskresults", "0002_auto_20151011_2248"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="taskresult",
            options={
                "ordering": ["created"],
                "verbose_name": "Task result",
                "verbose_name_plural": "Task results",
            },
        ),
        migrations.AddField(
            model_name="taskresult",
            name="created",
            field=model_utils.fields.AutoCreatedField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="created",
            ),
        ),
        migrations.AddField(
            model_name="taskresult",
            name="modified",
            field=model_utils.fields.AutoLastModifiedField(
                default=django.utils.timezone.now,
                editable=False,
                verbose_name="modified",
            ),
        ),
    ]
