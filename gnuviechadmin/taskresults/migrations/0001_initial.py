# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name="TaskResult",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("task_id", models.CharField(max_length=36, verbose_name="Task id")),
                (
                    "task_name",
                    models.CharField(max_length=64, verbose_name="Task name"),
                ),
                ("result", models.TextField(verbose_name="Task result")),
                ("finished", models.BooleanField(default=False)),
                ("state", models.CharField(max_length=16, verbose_name="Task state")),
            ],
            options={
                "verbose_name": "Task result",
                "verbose_name_plural": "Task results",
            },
            bases=(models.Model,),
        ),
    ]
