"""
This module defines Celery tasks to manage MySQL users and databases.

"""
from __future__ import absolute_import

from celery import shared_task


@shared_task
def create_mysql_user(username, password):
    """
    This task creates a new MySQL user.

    :param str username: the user name
    :param str password: the password
    :return: the created user's name
    :rtype: str

    """


@shared_task
def set_mysql_userpassword(username, password):
    """
    Set a new password for an existing MySQL user.

    :param str username: the user name
    :param str password: the password
    :return: True if the password could be set, False otherwise
    :rtype: boolean

    """


@shared_task
def delete_mysql_user(username):
    """
    This task deletes an existing MySQL user.

    :param str username: the user name
    :return: True if the user has been deleted, False otherwise
    :rtype: boolean

    """


@shared_task
def create_mysql_database(dbname, username):
    """
    This task creates a new MySQL database for the given MySQL user.

    :param str dbname: database name
    :param str username: the user name of an existing MySQL user
    :return: the database name
    :rtype: str

    """


@shared_task
def delete_mysql_database(dbname, username):
    """
    This task deletes an existing MySQL database and revokes privileges of the
    given user on that database.

    :param str dbname: database name
    :param str username: the user name of an existing MySQL user
    :return: True if the database has been deleted, False otherwise
    :rtype: boolean

    """
