"""
This module defines the URL patterns for user database views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import AddUserDatabase, ChangeDatabaseUserPassword, DeleteUserDatabase

urlpatterns = [
    re_path(
        r"^(?P<package>\d+)/create$", AddUserDatabase.as_view(), name="add_userdatabase"
    ),
    re_path(
        r"^(?P<package>\d+)/(?P<slug>[\w0-9]+)/setpassword",
        ChangeDatabaseUserPassword.as_view(),
        name="change_dbuser_password",
    ),
    re_path(
        r"^(?P<package>\d+)/(?P<slug>[\w0-9]+)/delete",
        DeleteUserDatabase.as_view(),
        name="delete_userdatabase",
    ),
]
