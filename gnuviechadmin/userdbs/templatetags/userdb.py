"""
This is the template tag library for user databases.

"""

from django import template
from django.utils.translation import gettext_lazy as _

from userdbs.models import DB_TYPES

register = template.Library()

_TYPE_NAME_MAP = {
    DB_TYPES.mysql: 'mysql',
    DB_TYPES.pgsql: 'postgres',
}


def db_type_icon_class(context):
    """
    This template tag derives the matching icon name for the numeric database
    type stored in the context variable db_type.

    The icon names used are those of `Font Mfizz
    <http://mfizz.com/oss/font-mfizz>`_.

    :param context: the template context
    :return: icon name
    :rtype: str

    """
    db_type = context['db_type']
    if db_type in _TYPE_NAME_MAP:
        return 'icon-' + _TYPE_NAME_MAP[db_type]
    return 'icon-database'


register.simple_tag(db_type_icon_class, takes_context=True)


def db_type_name(context):
    """
    This template tag gets the human readable database type for the numeric
    database type stored in the context variable db_type.

    :param context: the template context
    :return: human readable database type name
    :rtype: str

    """
    db_type = context['db_type']
    return _(DB_TYPES[db_type])


register.simple_tag(db_type_name, takes_context=True)
