"""
This module defines views for user database handling.

"""
from __future__ import absolute_import

from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import redirect
from django.utils.translation import gettext as _
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from gvacommon.viewmixins import StaffOrSelfLoginRequiredMixin

from gvawebcore.views import HostingPackageAndCustomerMixin

from .forms import AddUserDatabaseForm, ChangeDatabaseUserPasswordForm
from .models import DB_TYPES, DatabaseUser, UserDatabase


class AddUserDatabase(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, CreateView
):
    """
    This view is used to setup new user databases.

    """

    model = UserDatabase
    context_object_name = "database"
    template_name_suffix = "_create"
    form_class = AddUserDatabaseForm

    def _get_dbtypes(self, hostingpackage):
        retval = []
        db_options = hostingpackage.get_databases()
        for opt in db_options:
            dbs_of_type = UserDatabase.objects.filter(
                db_user__osuser=hostingpackage.osuser, db_user__db_type=opt["db_type"]
            ).count()
            if dbs_of_type < opt["number"]:
                retval.append((opt["db_type"], DB_TYPES[opt["db_type"]]))
        if len(retval) < 1:
            raise SuspiciousOperation(
                _("The hosting package has no database products assigned.")
            )
        return retval

    def get_form_kwargs(self):
        kwargs = super(AddUserDatabase, self).get_form_kwargs()
        kwargs["hostingpackage"] = self.get_hosting_package()
        kwargs["dbtypes"] = self._get_dbtypes(kwargs["hostingpackage"])
        return kwargs

    def form_valid(self, form):
        userdatabase = form.save()
        messages.success(
            self.request,
            _(
                "Successfully create new {type} database {dbname} for user " "{dbuser}."
            ).format(
                type=userdatabase.db_user.db_type,
                dbname=userdatabase.db_name,
                dbuser=userdatabase.db_user,
            ),
        )
        return redirect(self.get_hosting_package())


class ChangeDatabaseUserPassword(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, UpdateView
):
    """
    This view is used to change a database user's password.

    """

    model = DatabaseUser
    slug_field = "name"
    context_object_name = "dbuser"
    template_name_suffix = "_setpassword"
    form_class = ChangeDatabaseUserPasswordForm

    def get_form_kwargs(self):
        kwargs = super(ChangeDatabaseUserPassword, self).get_form_kwargs()
        kwargs["hostingpackage"] = self.get_hosting_package()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ChangeDatabaseUserPassword, self).get_context_data(**kwargs)
        context["hostingpackage"] = self.get_hosting_package()
        context["customer"] = self.get_customer_object()
        return context

    def form_valid(self, form):
        db_user = form.save()
        messages.success(
            self.request,
            _("Successfully changed password of database user {dbuser}.").format(
                dbuser=db_user.name
            ),
        )
        return redirect(self.get_hosting_package())


class DeleteUserDatabase(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, DeleteView
):
    """
    This view is used to delete user databases and databases users if they have
    no more databases assigned.

    """

    model = UserDatabase
    slug_field = "db_name"
    context_object_name = "database"

    def get_context_data(self, **kwargs):
        context = super(DeleteUserDatabase, self).get_context_data(**kwargs)
        context.update(
            {
                "hostingpackage": self.get_hosting_package(),
                "customer": self.get_customer_object(),
            }
        )
        return context

    def get_success_url(self):
        messages.success(
            self.request,
            _("Database deleted."),
        )
        return self.get_hosting_package().get_absolute_url()
