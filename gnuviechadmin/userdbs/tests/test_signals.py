"""
This module contains explicit tests for corner cases in
:py:mod:`userdbs.signals` that are not handled by the tests in
:py:mod:`userdbs.tests.test_models`.

"""
from unittest.mock import Mock

from django.test import TestCase
from django.test.utils import override_settings

from taskresults.models import TaskResult
from userdbs.signals import (
    handle_dbuser_created,
    handle_dbuser_deleted,
    handle_dbuser_password_set,
    handle_userdb_created,
    handle_userdb_deleted,
)


@override_settings(
    CELERY_ALWAYS_EAGER=True, CELERY_CACHE_BACKEND="memory", BROKER_BACKEND="memory"
)
class TestCaseWithCeleryTasks(TestCase):
    pass


class TestWithUnknownDBType(TestCaseWithCeleryTasks):
    def test_handle_dbuser_password_set_unknown(self):
        instance = Mock(data={"name": "test", "db_type": -1})
        handle_dbuser_password_set(Mock(name="sender"), instance, "secret")
        self.assertFalse(TaskResult.objects.exists())

    def test_handle_dbuser_create_unknown(self):
        instance = Mock(data={"name": "test", "db_type": -1})
        handle_dbuser_created(Mock(name="sender"), instance, True, password="secret")
        self.assertFalse(TaskResult.objects.exists())

    def test_handle_dbuser_deleted_unknown(self):
        instance = Mock(data={"name": "test", "db_type": -1})
        handle_dbuser_deleted(Mock(name="sender"), instance)
        self.assertFalse(TaskResult.objects.exists())

    def test_handle_userdb_created_unknown(self):
        instance = Mock(
            data={
                "db_name": "test",
                "db_user": Mock(data={"name": "test", "db_type": -1}),
            }
        )
        handle_userdb_created(Mock(name="sender"), instance, True)
        self.assertFalse(TaskResult.objects.exists())

    def test_handle_userdb_deleted_unknown(self):
        instance = Mock(
            data={
                "db_name": "test",
                "db_user": Mock(data={"name": "test", "db_type": -1}),
            }
        )
        handle_userdb_deleted(Mock(name="sender"), instance)
        self.assertFalse(TaskResult.objects.exists())
