"""
This module provides tests for the functions in
:py:mod:`userdbs.templatetags.userdb`.

"""
from unittest import TestCase

from django.utils.translation import gettext as _

from userdbs.models import DB_TYPES
from userdbs.templatetags.userdb import db_type_icon_class, db_type_name


class UserdbTemplateTagTests(TestCase):
    """
    Test suite for :py:mod:`userdbs.templatetags.userdb` functions.

    """

    def test_db_type_icon_class_unknown(self):
        self.assertEqual(db_type_icon_class({"db_type": "unknown"}), "icon-database")

    def test_db_type_icon_class_mysql(self):
        self.assertEqual(db_type_icon_class({"db_type": DB_TYPES.mysql}), "icon-mysql")

    def test_db_type_icon_class_pgsql(self):
        self.assertEqual(
            db_type_icon_class({"db_type": DB_TYPES.pgsql}), "icon-postgres"
        )

    def test_db_type_name_mysql(self):
        self.assertEqual(
            db_type_name({"db_type": DB_TYPES.mysql}), _(DB_TYPES[DB_TYPES.mysql])
        )

    def test_db_type_name_pgsql(self):
        self.assertEqual(
            db_type_name({"db_type": DB_TYPES.pgsql}), _(DB_TYPES[DB_TYPES.pgsql])
        )
