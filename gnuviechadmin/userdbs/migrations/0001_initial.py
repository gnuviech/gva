# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0004_auto_20150104_1751"),
    ]

    operations = [
        migrations.CreateModel(
            name="DatabaseUser",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                ("name", models.CharField(max_length=63, verbose_name="username")),
                (
                    "db_type",
                    models.PositiveSmallIntegerField(
                        verbose_name="database type",
                        choices=[(0, "PostgreSQL"), (1, "MySQL")],
                    ),
                ),
                (
                    "osuser",
                    models.ForeignKey(to="osusers.User", on_delete=models.CASCADE),
                ),
            ],
            options={
                "verbose_name": "database user",
                "verbose_name_plural": "database users",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="UserDatabase",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "db_name",
                    models.CharField(max_length=63, verbose_name="database name"),
                ),
                (
                    "db_user",
                    models.ForeignKey(
                        verbose_name="database user",
                        to="userdbs.DatabaseUser",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "user database",
                "verbose_name_plural": "user specific database",
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="userdatabase",
            unique_together={("db_name", "db_user")},
        ),
        migrations.AlterUniqueTogether(
            name="databaseuser",
            unique_together={("name", "db_type")},
        ),
    ]
