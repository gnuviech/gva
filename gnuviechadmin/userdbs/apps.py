"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`userdbs` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UserdbsAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`userdbs` app.

    """

    name = "userdbs"
    verbose_name = _("Database Users and their Databases")

    def ready(self):
        """
        Takes care of importing the signal handlers of the :py:mod:`userdbs`
        app.

        """
        import userdbs.signals  # NOQA
