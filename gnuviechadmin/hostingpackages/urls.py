"""
This module defines the URL patterns for hosting package related views.

"""
from __future__ import absolute_import

from django.urls import path, re_path

from .views import (
    AddHostingOption,
    AllCustomerHostingPackageList,
    CreateCustomerHostingPackage,
    CreateHostingPackage,
    CustomerHostingPackageDetails,
    CustomerHostingPackageDiskUsageDetails,
    HostingOptionChoices,
    UploadCustomerPackageDiskUsage,
)

urlpatterns = [
    re_path(r"^create$", CreateHostingPackage.as_view(), name="create_hosting_package"),
    re_path(
        r"^allpackages/",
        AllCustomerHostingPackageList.as_view(),
        name="all_hosting_packages",
    ),
    re_path(
        r"^(?P<user>[-\w0-9@.+_]+)/create$",
        CreateCustomerHostingPackage.as_view(),
        name="create_customer_hosting_package",
    ),
    re_path(
        r"^(?P<user>[-\w0-9@.+_]+)/(?P<pk>\d+)/$",
        CustomerHostingPackageDetails.as_view(),
        name="hosting_package_details",
    ),
    re_path(
        r"^(?P<pk>\d+)/option-choices$",
        HostingOptionChoices.as_view(),
        name="hosting_option_choices",
    ),
    re_path(
        r"^(?P<package>\d+)/add-option/(?P<type>\w+)/(?P<optionid>\d+)$",
        AddHostingOption.as_view(),
        name="add_hosting_option",
    ),
    path(
        "<str:user>/<int:package>/disk-usage/",
        CustomerHostingPackageDiskUsageDetails.as_view(),
        name="disk_usage_details",
    ),
    path(
        "upload-disk-usage/",
        UploadCustomerPackageDiskUsage.as_view(),
        name="upload_disk_usage",
    ),
]
