from rest_framework import serializers

from hostingpackages.models import CustomerPackageDiskUsage


class DiskUsageSerializer(serializers.Serializer):
    user = serializers.CharField()
