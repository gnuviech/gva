# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("domains", "0002_auto_20150124_1909"),
        ("hostingpackages", "0003_auto_20150118_1407"),
    ]

    operations = [
        migrations.CreateModel(
            name="CustomerHostingPackageDomain",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "domain",
                    models.OneToOneField(
                        verbose_name="hosting domain",
                        to="domains.HostingDomain",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "hosting_package",
                    models.ForeignKey(
                        related_name="domains",
                        verbose_name="hosting package",
                        to="hostingpackages.CustomerHostingPackage",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=(models.Model,),
        ),
    ]
