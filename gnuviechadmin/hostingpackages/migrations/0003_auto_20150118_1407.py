# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0002_auto_20150118_1319"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customermailboxoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="mailbox option template",
                to="hostingpackages.MailboxOption",
                help_text="The mailbox option template that this mailbox "
                "option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
