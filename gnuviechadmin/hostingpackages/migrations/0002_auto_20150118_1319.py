# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0001_squashed_0005_auto_20150118_1303"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="hostingoption",
            options={},
        ),
    ]
