# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0004_auto_20150104_1751"),
        ("hostingpackages", "0003_auto_20150118_1221"),
    ]

    operations = [
        migrations.AddField(
            model_name="customerhostingpackage",
            name="osuser",
            field=models.ForeignKey(
                verbose_name="Operating system user",
                blank=True,
                to="osusers.User",
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
