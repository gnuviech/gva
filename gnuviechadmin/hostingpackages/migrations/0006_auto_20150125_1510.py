# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0005_auto_20150125_1508"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="userdatabaseoption",
            options={},
        ),
        migrations.AlterUniqueTogether(
            name="customeruserdatabaseoption",
            unique_together=set([]),
        ),
    ]
