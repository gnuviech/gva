# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0002_auto_20150118_1149"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customerhostingpackage",
            name="name",
            field=models.CharField(max_length=128, verbose_name="name"),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="customerhostingpackage",
            unique_together=set([("customer", "name")]),
        ),
    ]
