# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="CustomerHostingPackage",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "name",
                    models.CharField(unique=True, max_length=128, verbose_name="name"),
                ),
                (
                    "description",
                    models.TextField(verbose_name="description", blank=True),
                ),
                (
                    "mailboxcount",
                    models.PositiveIntegerField(verbose_name="mailbox count"),
                ),
                (
                    "diskspace",
                    models.PositiveIntegerField(
                        help_text="disk space for the hosting package",
                        verbose_name="disk space",
                    ),
                ),
                (
                    "diskspace_unit",
                    models.PositiveSmallIntegerField(
                        verbose_name="unit of disk space",
                        choices=[(0, "MiB"), (1, "GiB"), (2, "TiB")],
                    ),
                ),
                (
                    "customer",
                    models.ForeignKey(
                        verbose_name="customer",
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "customer hosting package",
                "verbose_name_plural": "customer hosting packages",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="CustomerHostingPackageOption",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
            ],
            options={
                "verbose_name": "customer hosting option",
                "verbose_name_plural": "customer hosting options",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="CustomerDiskSpaceOption",
            fields=[
                (
                    "customerhostingpackageoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.CustomerHostingPackageOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                ("diskspace", models.PositiveIntegerField(verbose_name="disk space")),
                (
                    "diskspace_unit",
                    models.PositiveSmallIntegerField(
                        verbose_name="unit of disk space",
                        choices=[(0, "MiB"), (1, "GiB"), (2, "TiB")],
                    ),
                ),
            ],
            options={
                "ordering": ["diskspace_unit", "diskspace"],
                "abstract": False,
                "verbose_name": "Disk space option",
                "verbose_name_plural": "Disk space options",
            },
            bases=("hostingpackages.customerhostingpackageoption", models.Model),
        ),
        migrations.CreateModel(
            name="CustomerMailboxOption",
            fields=[
                (
                    "customerhostingpackageoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.CustomerHostingPackageOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "number",
                    models.PositiveIntegerField(
                        unique=True, verbose_name="number of mailboxes"
                    ),
                ),
            ],
            options={
                "ordering": ["number"],
                "abstract": False,
                "verbose_name": "Mailbox option",
                "verbose_name_plural": "Mailbox options",
            },
            bases=("hostingpackages.customerhostingpackageoption", models.Model),
        ),
        migrations.CreateModel(
            name="CustomerUserDatabaseOption",
            fields=[
                (
                    "customerhostingpackageoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.CustomerHostingPackageOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "number",
                    models.PositiveIntegerField(
                        default=1, verbose_name="number of databases"
                    ),
                ),
                (
                    "db_type",
                    models.PositiveSmallIntegerField(
                        verbose_name="database type",
                        choices=[(0, "PostgreSQL"), (1, "MySQL")],
                    ),
                ),
            ],
            options={
                "ordering": ["db_type", "number"],
                "abstract": False,
                "verbose_name": "Database option",
                "verbose_name_plural": "Database options",
            },
            bases=("hostingpackages.customerhostingpackageoption", models.Model),
        ),
        migrations.CreateModel(
            name="HostingOption",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
            ],
            options={
                "verbose_name": "Hosting option",
                "verbose_name_plural": "Hosting options",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="DiskSpaceOption",
            fields=[
                (
                    "hostingoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.HostingOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                ("diskspace", models.PositiveIntegerField(verbose_name="disk space")),
                (
                    "diskspace_unit",
                    models.PositiveSmallIntegerField(
                        verbose_name="unit of disk space",
                        choices=[(0, "MiB"), (1, "GiB"), (2, "TiB")],
                    ),
                ),
            ],
            options={
                "ordering": ["diskspace_unit", "diskspace"],
                "abstract": False,
                "verbose_name": "Disk space option",
                "verbose_name_plural": "Disk space options",
            },
            bases=("hostingpackages.hostingoption", models.Model),
        ),
        migrations.CreateModel(
            name="HostingPackageTemplate",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "name",
                    models.CharField(unique=True, max_length=128, verbose_name="name"),
                ),
                (
                    "description",
                    models.TextField(verbose_name="description", blank=True),
                ),
                (
                    "mailboxcount",
                    models.PositiveIntegerField(verbose_name="mailbox count"),
                ),
                (
                    "diskspace",
                    models.PositiveIntegerField(
                        help_text="disk space for the hosting package",
                        verbose_name="disk space",
                    ),
                ),
                (
                    "diskspace_unit",
                    models.PositiveSmallIntegerField(
                        verbose_name="unit of disk space",
                        choices=[(0, "MiB"), (1, "GiB"), (2, "TiB")],
                    ),
                ),
            ],
            options={
                "verbose_name": "Hosting package",
                "verbose_name_plural": "Hosting packages",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="MailboxOption",
            fields=[
                (
                    "hostingoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.HostingOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "number",
                    models.PositiveIntegerField(
                        unique=True, verbose_name="number of mailboxes"
                    ),
                ),
            ],
            options={
                "ordering": ["number"],
                "abstract": False,
                "verbose_name": "Mailbox option",
                "verbose_name_plural": "Mailbox options",
            },
            bases=("hostingpackages.hostingoption", models.Model),
        ),
        migrations.CreateModel(
            name="UserDatabaseOption",
            fields=[
                (
                    "hostingoption_ptr",
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to="hostingpackages.HostingOption",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "number",
                    models.PositiveIntegerField(
                        default=1, verbose_name="number of databases"
                    ),
                ),
                (
                    "db_type",
                    models.PositiveSmallIntegerField(
                        verbose_name="database type",
                        choices=[(0, "PostgreSQL"), (1, "MySQL")],
                    ),
                ),
            ],
            options={
                "ordering": ["db_type", "number"],
                "abstract": False,
                "verbose_name": "Database option",
                "verbose_name_plural": "Database options",
            },
            bases=("hostingpackages.hostingoption", models.Model),
        ),
        migrations.AlterUniqueTogether(
            name="userdatabaseoption",
            unique_together={("number", "db_type")},
        ),
        migrations.AlterUniqueTogether(
            name="diskspaceoption",
            unique_together={("diskspace", "diskspace_unit")},
        ),
        migrations.AddField(
            model_name="customeruserdatabaseoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="user database option template",
                to="hostingpackages.UserDatabaseOption",
                help_text="The user database option template that this "
                "hosting option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="customeruserdatabaseoption",
            unique_together={("number", "db_type")},
        ),
        migrations.AddField(
            model_name="customermailboxoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="mailbox option template",
                to="hostingpackages.UserDatabaseOption",
                help_text="The mailbox option template that this hosting "
                "option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="customerhostingpackageoption",
            name="hosting_package",
            field=models.ForeignKey(
                verbose_name="hosting package",
                to="hostingpackages.CustomerHostingPackage",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="customerhostingpackage",
            name="template",
            field=models.ForeignKey(
                verbose_name="hosting package template",
                to="hostingpackages.HostingPackageTemplate",
                help_text="The hosting package template that this hosting "
                "package is based on.",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="customerdiskspaceoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="disk space option template",
                to="hostingpackages.DiskSpaceOption",
                help_text="The disk space option template that this hosting "
                "option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="customerdiskspaceoption",
            unique_together={("diskspace", "diskspace_unit")},
        ),
    ]
