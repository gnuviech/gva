# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0004_customerhostingpackagedomain"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="diskspaceoption",
            options={},
        ),
        migrations.AlterUniqueTogether(
            name="customerdiskspaceoption",
            unique_together=set([]),
        ),
    ]
