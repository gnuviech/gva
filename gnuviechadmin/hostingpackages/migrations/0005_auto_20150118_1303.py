# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0004_customerhostingpackage_osuser"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customerhostingpackage",
            name="osuser",
            field=models.OneToOneField(
                null=True,
                blank=True,
                to="osusers.User",
                verbose_name="Operating system user",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
