# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("hostingpackages", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="customerdiskspaceoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="disk space option template",
                to="hostingpackages.DiskSpaceOption",
                help_text="The disk space option template that this disk "
                "space option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="customerhostingpackage",
            name="template",
            field=models.ForeignKey(
                verbose_name="hosting package template",
                to="hostingpackages.HostingPackageTemplate",
                help_text="The hosting package template that this hosting "
                "package is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="customermailboxoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="mailbox option template",
                to="hostingpackages.UserDatabaseOption",
                help_text="The mailbox option template that this mailbox "
                "option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="customeruserdatabaseoption",
            name="template",
            field=models.ForeignKey(
                verbose_name="user database option template",
                to="hostingpackages.UserDatabaseOption",
                help_text="The user database option template that this "
                "database option is based on",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
