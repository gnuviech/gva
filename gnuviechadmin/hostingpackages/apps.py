"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`hostingpackages` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HostingPackagesAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`hostingpackages` app.

    """

    name = "hostingpackages"
    verbose_name = _("Hosting Packages and Options")
