"""
This module contains the form classes related to hosting packages.

"""
from __future__ import absolute_import

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms
from django.urls import reverse
from django.utils.translation import gettext as _

from .models import (
    CustomerDiskSpaceOption,
    CustomerHostingPackage,
    CustomerMailboxOption,
    CustomerUserDatabaseOption,
)


class CreateCustomerHostingPackageForm(forms.ModelForm):
    """
    This form class is used for creating new customer hosting packages with
    a preselected customer.

    """

    class Meta:
        model = CustomerHostingPackage
        fields = ["template", "name", "description"]

    def __init__(self, instance, *args, **kwargs):
        username = kwargs.pop("user")
        super(CreateCustomerHostingPackageForm, self).__init__(*args, **kwargs)
        self.fields["description"].widget.attrs["rows"] = 2
        self.helper = FormHelper()
        self.helper.form_action = reverse(
            "create_customer_hosting_package", kwargs={"user": username}
        )
        self.helper.layout = Layout(
            "template",
            "name",
            "description",
            Submit("submit", _("Add Hosting Package")),
        )


class CreateHostingPackageForm(forms.ModelForm):
    """
    This form class is used for creating new customer hosting packages.

    """

    class Meta:
        model = CustomerHostingPackage
        fields = ["customer", "template", "name", "description"]

    def __init__(self, instance, *args, **kwargs):
        super(CreateHostingPackageForm, self).__init__(*args, **kwargs)
        self.fields["description"].widget.attrs["rows"] = 2
        self.helper = FormHelper()
        self.helper.form_action = reverse("create_hosting_package")
        self.helper.layout = Layout(
            "customer",
            "template",
            "name",
            "description",
            Submit("submit", _("Add Hosting Package")),
        )


class AddDiskspaceOptionForm(forms.ModelForm):
    class Meta:
        model = CustomerDiskSpaceOption
        fields = ["diskspace", "diskspace_unit"]

    def __init__(self, *args, **kwargs):
        self.hostingpackage = kwargs.pop("hostingpackage")
        self.option_template = kwargs.pop("option_template")
        super(AddDiskspaceOptionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse(
            "add_hosting_option",
            kwargs={
                "package": self.hostingpackage.id,
                "type": "diskspace",
                "optionid": self.option_template.id,
            },
        )
        self.helper.add_input(Submit("submit", _("Add disk space option")))

    def save(self, commit=True):
        self.instance.hosting_package = self.hostingpackage
        self.instance.template = self.option_template
        return super(AddDiskspaceOptionForm, self).save(commit=True)


class AddMailboxOptionForm(forms.ModelForm):
    class Meta:
        model = CustomerMailboxOption
        fields = ["number"]

    def __init__(self, *args, **kwargs):
        self.hostingpackage = kwargs.pop("hostingpackage")
        self.option_template = kwargs.pop("option_template")
        super(AddMailboxOptionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse(
            "add_hosting_option",
            kwargs={
                "package": self.hostingpackage.id,
                "type": "mailboxes",
                "optionid": self.option_template.id,
            },
        )
        self.helper.add_input(Submit("submit", _("Add mailbox option")))

    def save(self, commit=True):
        self.instance.hosting_package = self.hostingpackage
        self.instance.template = self.option_template
        return super(AddMailboxOptionForm, self).save(commit=True)


class AddUserDatabaseOptionForm(forms.ModelForm):
    class Meta:
        model = CustomerUserDatabaseOption
        fields = ["number"]

    def __init__(self, *args, **kwargs):
        self.hostingpackage = kwargs.pop("hostingpackage")
        self.option_template = kwargs.pop("option_template")
        super(AddUserDatabaseOptionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_action = reverse(
            "add_hosting_option",
            kwargs={
                "package": self.hostingpackage.id,
                "type": "databases",
                "optionid": self.option_template.id,
            },
        )
        self.helper.add_input(Submit("submit", _("Add database option")))

    def save(self, commit=True):
        self.instance.hosting_package = self.hostingpackage
        self.instance.template = self.option_template
        self.instance.db_type = self.option_template.db_type
        return super(AddUserDatabaseOptionForm, self).save(commit=True)
