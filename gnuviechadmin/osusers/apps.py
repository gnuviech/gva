"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`osusers` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class OsusersAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`osusers` app.

    """

    name = "osusers"
    verbose_name = _("Operating System Users and Groups")

    def ready(self):
        """
        Takes care of importing the signal handlers of the :py:mod:`osusers`
        app.

        """
        import osusers.signals  # NOQA
