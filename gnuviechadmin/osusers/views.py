"""
This module defines the views for gnuviechadmin operating system user handling.

"""
from __future__ import absolute_import

from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from gvacommon.viewmixins import StaffOrSelfLoginRequiredMixin

from gvawebcore.views import HostingPackageAndCustomerMixin

from .forms import (
    AddSshPublicKeyForm,
    ChangeOsUserPasswordForm,
    EditSshPublicKeyCommentForm,
)
from .models import SshPublicKey, User


class SetOsUserPassword(StaffOrSelfLoginRequiredMixin, UpdateView):
    """
    This view is used for setting a new operating system user password.

    """

    model = User
    slug_field = "username"
    template_name_suffix = "_setpassword"
    context_object_name = "osuser"
    form_class = ChangeOsUserPasswordForm

    def get_customer_object(self):
        return self.get_object().customer

    def get_context_data(self, *args, **kwargs):
        context = super(SetOsUserPassword, self).get_context_data(*args, **kwargs)
        context["customer"] = self.get_customer_object()
        return context

    def form_valid(self, form):
        osuser = form.save()
        messages.success(
            self.request,
            _("New password for {username} has been set successfully.").format(
                username=osuser.username
            ),
        )
        return redirect(osuser.customerhostingpackage)


class AddSshPublicKey(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, CreateView
):
    """
    This view is used to add an SSH key for an existing hosting account's
    operating system user.

    """

    model = SshPublicKey
    context_object_name = "key"
    template_name_suffix = "_create"
    form_class = AddSshPublicKeyForm

    def get_form_kwargs(self):
        kwargs = super(AddSshPublicKey, self).get_form_kwargs()
        kwargs["hostingpackage"] = self.get_hosting_package()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AddSshPublicKey, self).get_context_data(**kwargs)
        context.update(
            {
                "customer": self.get_customer_object(),
                "osuser": self.get_hosting_package().osuser.username,
            }
        )
        return context

    def form_valid(self, form):
        key = form.save()
        messages.success(
            self.request,
            _("Successfully added new {algorithm} SSH public key.").format(
                algorithm=key.algorithm
            ),
        )
        return redirect(self.get_hosting_package())


class ListSshPublicKeys(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, ListView
):
    """
    This view is used for showing the list of :py:class:`SSH public keys
    <osusers.models.SshPublicKey>` assigned to the hosting package specified
    via URL parameter 'pattern'.

    """

    model = SshPublicKey
    context_object_name = "keys"

    def get_queryset(self):
        return SshPublicKey.objects.filter(user=self.get_hosting_package().osuser)

    def get_context_data(self, **kwargs):
        context = super(ListSshPublicKeys, self).get_context_data(**kwargs)
        context.update(
            {
                "hostingpackage": self.get_hosting_package(),
                "customer": self.get_customer_object(),
                "osuser": self.get_hosting_package().osuser.username,
            }
        )
        return context


class DeleteSshPublicKey(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, DeleteView
):
    """
    This view is used for delete confirmation of a :py:class:`SSH public key
    <osusers.models.SshPublicKey>`.

    """

    model = SshPublicKey
    context_object_name = "key"

    def get_queryset(self):
        return (
            super(DeleteSshPublicKey, self)
            .get_queryset()
            .filter(user=self.get_hosting_package().osuser)
        )

    def get_context_data(self, **kwargs):
        context = super(DeleteSshPublicKey, self).get_context_data(**kwargs)
        context.update(
            {
                "hostingpackage": self.get_hosting_package(),
                "customer": self.get_customer_object(),
                "osuser": self.get_hosting_package().osuser.username,
            }
        )
        return context

    def get_success_url(self):
        return reverse(
            "list_ssh_keys", kwargs={"package": self.get_hosting_package().id}
        )


class EditSshPublicKeyComment(
    HostingPackageAndCustomerMixin, StaffOrSelfLoginRequiredMixin, UpdateView
):
    """
    This view is used for editing the comment field of a :py:class:`SSH public
    key <osusers.models.SshPublicKey>`.

    """

    model = SshPublicKey
    context_object_name = "key"
    template_name_suffix = "_edit_comment"
    form_class = EditSshPublicKeyCommentForm

    def get_queryset(self):
        return (
            super(EditSshPublicKeyComment, self)
            .get_queryset()
            .filter(user=self.get_hosting_package().osuser)
        )

    def get_form_kwargs(self):
        kwargs = super(EditSshPublicKeyComment, self).get_form_kwargs()
        kwargs["hostingpackage"] = self.get_hosting_package()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EditSshPublicKeyComment, self).get_context_data(**kwargs)
        context.update(
            {
                "hostingpackage": self.get_hosting_package(),
                "customer": self.get_customer_object(),
                "osuser": self.get_hosting_package().osuser.username,
            }
        )
        return context

    def get_success_url(self):
        return reverse(
            "list_ssh_keys", kwargs={"package": self.get_hosting_package().id}
        )
