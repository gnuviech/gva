"""
This module defines the URL patterns for operating system user related views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import (
    AddSshPublicKey,
    DeleteSshPublicKey,
    EditSshPublicKeyComment,
    ListSshPublicKeys,
    SetOsUserPassword,
)

urlpatterns = [
    re_path(
        r"^(?P<slug>[\w0-9@.+-_]+)/setpassword$",
        SetOsUserPassword.as_view(),
        name="set_osuser_password",
    ),
    re_path(
        r"^(?P<package>\d+)/ssh-keys/$",
        ListSshPublicKeys.as_view(),
        name="list_ssh_keys",
    ),
    re_path(
        r"^(?P<package>\d+)/ssh-keys/add$",
        AddSshPublicKey.as_view(),
        name="add_ssh_key",
    ),
    re_path(
        r"^(?P<package>\d+)/ssh-keys/(?P<pk>\d+)/edit-comment$",
        EditSshPublicKeyComment.as_view(),
        name="edit_ssh_key_comment",
    ),
    re_path(
        r"^(?P<package>\d+)/ssh-keys/(?P<pk>\d+)/delete$",
        DeleteSshPublicKey.as_view(),
        name="delete_ssh_key",
    ),
]
