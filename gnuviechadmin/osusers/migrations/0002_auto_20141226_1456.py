# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0001_initial"),
    ]

    operations = [
        migrations.DeleteModel(
            name="DeleteTaskResult",
        ),
        migrations.RemoveField(
            model_name="grouptaskresult",
            name="group",
        ),
        migrations.DeleteModel(
            name="GroupTaskResult",
        ),
        migrations.RemoveField(
            model_name="usertaskresult",
            name="user",
        ),
        migrations.DeleteModel(
            name="UserTaskResult",
        ),
    ]
