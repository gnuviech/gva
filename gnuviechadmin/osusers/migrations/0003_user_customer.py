# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("osusers", "0002_auto_20141226_1456"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="customer",
            field=models.ForeignKey(
                default=1, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
            preserve_default=False,
        ),
    ]
