# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name="AdditionalGroup",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
            ],
            options={
                "verbose_name": "Additional group",
                "verbose_name_plural": "Additional groups",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="DeleteTaskResult",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "task_uuid",
                    models.CharField(max_length=64, serialize=False, primary_key=True),
                ),
                ("task_name", models.CharField(max_length=255, db_index=True)),
                ("is_finished", models.BooleanField(default=False)),
                ("is_success", models.BooleanField(default=False)),
                ("state", models.CharField(max_length=10)),
                ("result_body", models.TextField(blank=True)),
                ("modeltype", models.CharField(max_length=20, db_index=True)),
                ("modelname", models.CharField(max_length=255)),
            ],
            options={
                "abstract": False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="Group",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "groupname",
                    models.CharField(
                        unique=True, max_length=16, verbose_name="Group name"
                    ),
                ),
                (
                    "gid",
                    models.PositiveSmallIntegerField(
                        unique=True,
                        serialize=False,
                        verbose_name="Group ID",
                        primary_key=True,
                    ),
                ),
                ("descr", models.TextField(verbose_name="Description", blank=True)),
                (
                    "passwd",
                    models.CharField(
                        max_length=128, verbose_name="Group password", blank=True
                    ),
                ),
            ],
            options={
                "verbose_name": "Group",
                "verbose_name_plural": "Groups",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="GroupTaskResult",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "task_uuid",
                    models.CharField(max_length=64, serialize=False, primary_key=True),
                ),
                ("task_name", models.CharField(max_length=255, db_index=True)),
                ("is_finished", models.BooleanField(default=False)),
                ("is_success", models.BooleanField(default=False)),
                ("state", models.CharField(max_length=10)),
                ("result_body", models.TextField(blank=True)),
                (
                    "group",
                    models.ForeignKey(to="osusers.Group", on_delete=models.CASCADE),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="User",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "username",
                    models.CharField(
                        unique=True, max_length=64, verbose_name="User name"
                    ),
                ),
                (
                    "uid",
                    models.PositiveSmallIntegerField(
                        unique=True,
                        serialize=False,
                        verbose_name="User ID",
                        primary_key=True,
                    ),
                ),
                (
                    "gecos",
                    models.CharField(
                        max_length=128, verbose_name="Gecos field", blank=True
                    ),
                ),
                (
                    "homedir",
                    models.CharField(max_length=256, verbose_name="Home directory"),
                ),
                ("shell", models.CharField(max_length=64, verbose_name="Login shell")),
            ],
            options={
                "verbose_name": "Benutzer",
                "verbose_name_plural": "Users",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="Shadow",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "user",
                    models.OneToOneField(
                        primary_key=True,
                        serialize=False,
                        to="osusers.User",
                        verbose_name="Benutzer",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "passwd",
                    models.CharField(max_length=128, verbose_name="Encrypted password"),
                ),
                (
                    "changedays",
                    models.PositiveSmallIntegerField(
                        help_text="This is expressed in days since Jan 1, 1970",
                        null=True,
                        verbose_name="Date of last change",
                        blank=True,
                    ),
                ),
                (
                    "minage",
                    models.PositiveSmallIntegerField(
                        help_text="Minimum number of days before the password can "
                        "be changed",
                        null=True,
                        verbose_name="Minimum age",
                        blank=True,
                    ),
                ),
                (
                    "maxage",
                    models.PositiveSmallIntegerField(
                        help_text="Maximum number of days after which the "
                        "password has to be changed",
                        null=True,
                        verbose_name="Maximum age",
                        blank=True,
                    ),
                ),
                (
                    "gracedays",
                    models.PositiveSmallIntegerField(
                        help_text="The number of days before the password is "
                        "going to expire",
                        null=True,
                        verbose_name="Grace period",
                        blank=True,
                    ),
                ),
                (
                    "inactdays",
                    models.PositiveSmallIntegerField(
                        help_text="The number of days after the password has "
                        "expired during which the password should still "
                        "be accepted",
                        null=True,
                        verbose_name="Inactivity period",
                        blank=True,
                    ),
                ),
                (
                    "expiredays",
                    models.PositiveSmallIntegerField(
                        default=None,
                        help_text="The date of expiration of the account, "
                        "expressed as number of days since Jan 1, 1970",
                        null=True,
                        verbose_name="Account expiration date",
                        blank=True,
                    ),
                ),
            ],
            options={
                "verbose_name": "Shadow password",
                "verbose_name_plural": "Shadow passwords",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name="UserTaskResult",
            fields=[
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "task_uuid",
                    models.CharField(max_length=64, serialize=False, primary_key=True),
                ),
                ("task_name", models.CharField(max_length=255, db_index=True)),
                ("is_finished", models.BooleanField(default=False)),
                ("is_success", models.BooleanField(default=False)),
                ("state", models.CharField(max_length=10)),
                ("result_body", models.TextField(blank=True)),
                (
                    "user",
                    models.ForeignKey(to="osusers.User", on_delete=models.CASCADE),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name="user",
            name="group",
            field=models.ForeignKey(
                verbose_name="Group", to="osusers.Group", on_delete=models.CASCADE
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="additionalgroup",
            name="group",
            field=models.ForeignKey(to="osusers.Group", on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name="additionalgroup",
            name="user",
            field=models.ForeignKey(to="osusers.User", on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name="additionalgroup",
            unique_together={("user", "group")},
        ),
    ]
