# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0003_user_customer"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="user",
            options={"verbose_name": "User", "verbose_name_plural": "Users"},
        ),
        migrations.AlterField(
            model_name="shadow",
            name="user",
            field=models.OneToOneField(
                primary_key=True,
                serialize=False,
                to="osusers.User",
                verbose_name="User",
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
    ]
