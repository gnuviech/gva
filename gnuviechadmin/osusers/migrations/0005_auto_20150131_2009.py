# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0004_auto_20150104_1751"),
    ]

    operations = [
        migrations.CreateModel(
            name="SshPublicKey",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "algorithm",
                    models.CharField(max_length=20, verbose_name="Algorithm"),
                ),
                (
                    "data",
                    models.TextField(
                        help_text="Base64 encoded key bytes", verbose_name="Key bytes"
                    ),
                ),
                ("comment", models.TextField(verbose_name="Comment", blank=True)),
                (
                    "user",
                    models.ForeignKey(
                        verbose_name="User", to="osusers.User", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "verbose_name": "SSH public key",
                "verbose_name_plural": "SSH public keys",
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="sshpublickey",
            unique_together={("user", "algorithm", "data")},
        ),
    ]
