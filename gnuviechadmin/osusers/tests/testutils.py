from django.contrib import auth

from osusers.models import User


def create_test_customer():
    customer_model = auth.get_user_model()
    return customer_model.objects.create_user("test_customer", "testuser@example.org", "test_password")


def create_test_user():
    customer = create_test_customer()
    return User.objects.create_user(customer)
