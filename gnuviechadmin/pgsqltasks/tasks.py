"""
This module defines Celery tasks to manage PostgreSQL users and databases.

"""
from __future__ import absolute_import

from celery import shared_task


@shared_task
def create_pgsql_user(username, password):
    """
    This task creates a new PostgreSQL user.

    :param str username: the user name
    :param str password: the password
    :return: the created user's name
    :rtype: str

    """


@shared_task
def set_pgsql_userpassword(username, password):
    """
    Set a new password for an existing PostgreSQL user.

    :param str username: the user name
    :param str password: the password
    :return: True if the password could be set, False otherwise
    :rtype: boolean

    """


@shared_task
def delete_pgsql_user(username):
    """
    This task deletes an existing PostgreSQL user.

    :param str username: the user name
    :return: True if the user has been deleted, False otherwise
    :rtype: boolean

    """


@shared_task
def create_pgsql_database(dbname, username):
    """
    This task creates a new PostgreSQL database for the given PostgreSQL user.

    :param str dbname: database name
    :param str username: the user name of an existing PostgreSQL user
    :return: the database name
    :rtype: str

    """


@shared_task
def delete_pgsql_database(dbname):
    """
    This task deletes an existing PostgreSQL database.

    :param str dbname: database name
    :return: True if the database has been deleted, False otherwise
    :rtype: boolean

    """
