"""
This module contains tests for :py:mod:`gvawebcore.forms`.

"""

from unittest import TestCase

from gvawebcore.forms import PASSWORD_MISMATCH_ERROR, PasswordModelFormMixin


class PasswordModelFormMixinTest(TestCase):
    def test_form_properties(self):
        form = PasswordModelFormMixin()
        self.assertIn("password1", form.fields)
        self.assertIn("password2", form.fields)

    def test_clean_password_same(self):
        form = PasswordModelFormMixin(
            data={"password1": "secret", "password2": "secret"}
        )
        self.assertTrue(form.is_valid())
        self.assertEqual("secret", form.clean_password2())

    def test_clean_password_different(self):
        form = PasswordModelFormMixin(
            data={"password1": "onesecret", "password2": "other"}
        )
        self.assertFalse(form.is_valid())
        self.assertIn("password2", form.errors)
        self.assertIn(PASSWORD_MISMATCH_ERROR, form.errors["password2"])
