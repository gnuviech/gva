"""
This model contains tests for :py:mod:`gvawebcore.views`.

"""

from unittest import TestCase
from unittest.mock import Mock, patch

from gvawebcore.views import HostingPackageAndCustomerMixin


class HostingPackageAndCustomerMixinTest(TestCase):
    class TestView(HostingPackageAndCustomerMixin):

        kwargs = {"package": "1"}

    @patch("gvawebcore.views.get_object_or_404")
    def test_get_hosting_package(self, get_object_or_404):
        get_object_or_404.return_value = "A package"
        view = self.TestView()
        self.assertEqual("A package", view.get_hosting_package())

    def test_get_hosting_package_cached(self):
        view = self.TestView()
        view.hostingpackage = "Cached package"
        self.assertEqual("Cached package", view.get_hosting_package())

    @patch("gvawebcore.views.get_object_or_404")
    def test_get_customer_object(self, get_object_or_404):
        get_object_or_404.return_value = Mock(customer="A customer")
        view = self.TestView()
        self.assertEqual("A customer", view.get_customer_object())
