"""
This module defines form classes that can be extended by other gnuviechadmin
apps' forms.

"""
from __future__ import absolute_import

from django import forms
from django.utils.translation import gettext_lazy as _

PASSWORD_MISMATCH_ERROR = _("Passwords don't match")
"""
Error message for non matching passwords.
"""


class PasswordModelFormMixin(forms.Form):
    """
    A form for entering a password in two password fields. The form checks
    whether both fields contain the same string.

    """

    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput,
    )

    def clean_password2(self):
        """
        Check that the two password entries match.

        :return: the validated password
        :rtype: str or None

        """
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(PASSWORD_MISMATCH_ERROR)
        return password2
