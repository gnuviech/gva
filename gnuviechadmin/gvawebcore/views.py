"""
This module defines common view code to be used by multiple gnuviechadmin apps.

"""
from __future__ import absolute_import

from django.shortcuts import get_object_or_404

from hostingpackages.models import CustomerHostingPackage


class HostingPackageAndCustomerMixin(object):
    """
    Mixin for views that gets the hosting package instance from the URL
    keyword argument 'package'.

    """

    hosting_package_kwarg = "package"
    """Keyword argument used to find the hosting package in the URL."""

    hostingpackage = None

    def get_hosting_package(self):
        if self.hostingpackage is None:
            self.hostingpackage = get_object_or_404(
                CustomerHostingPackage, pk=int(self.kwargs[self.hosting_package_kwarg])
            )
        return self.hostingpackage

    def get_customer_object(self):
        return self.get_hosting_package().customer
