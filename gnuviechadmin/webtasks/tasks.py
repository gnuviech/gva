"""
This module defines Celery tasks to manage website configurations.

"""
from __future__ import absolute_import

from celery import shared_task


@shared_task
def create_web_vhost_config(username, sitename, wildcard):
    """
    This task creates a virtual host configuration on an nginx web
    server.

    :param str username: user who owns the site
    :param str sitename: site name
    :param boolean wildcard: designates whether this is website has a wildcard
        subdomain
    :return: :py:const:`True` if the creation finished successfully
    :rtype: boolean

    """


@shared_task
def disable_web_vhost(sitename):
    """
    This task disables a virtual host configuration on an nginx web server.

    :param str sitename: site name
    :return: :py:const:`True` if the virtual host has been disabled
    :rtype: boolean

    """


@shared_task
def enable_web_vhost(sitename):
    """
    This task enables an existing virtual host configuration on an nginx web
    server.

    :param str sitename: site name
    :return: :py:const:`True` if the virtual host has been enabled
    :rtype: boolean

    """


@shared_task
def delete_web_vhost_config(sitename):
    """
    This task removes a virtual host configuration on an nginx web server.

    :param str sitename: site name
    :return: :py:const:`True` if the configuration has been deleted
    :rtype: boolean

    """


@shared_task
def create_web_php_fpm_pool_config(username):
    """
    This task creates a PHP FPM pool configuration.

    :param str username: user name
    :return: :py:const:`True` if the creation finished successfully
    :rtype: boolean

    """


@shared_task
def delete_web_php_fpm_pool_config(username):
    """
    This task deletes a PHP FPM pool configuration.

    :param str username: user name
    :return: :py:const:`True` if the pool has been deleted
    :rtype: boolean

    """
