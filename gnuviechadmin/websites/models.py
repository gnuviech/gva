"""
This module defines the database models for website handling.

"""
from __future__ import absolute_import

from django.db import models
from django.utils.translation import gettext as _

from domains.models import HostingDomain
from osusers.models import User as OsUser


class Website(models.Model):
    """
    This is the model for a website.

    """

    subdomain = models.CharField(_("sub domain"), max_length=64)
    osuser = models.ForeignKey(
        OsUser, verbose_name=_("operating system user"), on_delete=models.CASCADE
    )
    domain = models.ForeignKey(HostingDomain, models.CASCADE, verbose_name=_("domain"))
    wildcard = models.BooleanField(_("wildcard"), default=False)

    class Meta:
        unique_together = [("domain", "subdomain")]
        verbose_name = _("website")
        verbose_name_plural = _("websites")

    def __str__(self):
        return self.get_fqdn()

    def get_fqdn(self):
        return "{subdomain}.{domain}".format(
            subdomain=self.subdomain, domain=self.domain.domain
        )
