"""
Admin site for websites.

"""
from __future__ import absolute_import

from django.contrib import admin

from .models import Website


admin.site.register(Website)
