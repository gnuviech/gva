"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`websites` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class WebsitesAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`websites` app.

    """

    name = "websites"
    verbose_name = _("Websites")

    def ready(self):
        """
        Takes care of importing the signal handlers of the :py:mod:`websites`
        app.

        """
        import websites.signals  # NOQA
