"""
This module defines the URL patterns for website related views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import AddWebsite, DeleteWebsite

urlpatterns = [
    re_path(
        r"^(?P<package>\d+)/(?P<domain>[\w0-9.-]+)/create$",
        AddWebsite.as_view(),
        name="add_website",
    ),
    re_path(
        r"^(?P<package>\d+)/(?P<domain>[\w0-9.-]+)/(?P<pk>\d+)/delete$",
        DeleteWebsite.as_view(),
        name="delete_website",
    ),
]
