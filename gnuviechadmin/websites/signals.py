"""
This module contains the signal handlers of the :py:mod:`websites` app.

The module starts Celery_ tasks.

.. _Celery: https://www.celeryproject.org/

"""
import logging

from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from fileservertasks.tasks import (
    create_file_website_hierarchy,
    delete_file_website_hierarchy,
)
from taskresults.models import TaskResult
from websites.models import Website
from webtasks.tasks import (
    create_web_php_fpm_pool_config,
    create_web_vhost_config,
    delete_web_php_fpm_pool_config,
    delete_web_vhost_config,
    disable_web_vhost,
    enable_web_vhost,
)

_LOGGER = logging.getLogger(__name__)


@receiver(post_save, sender=Website)
def handle_website_created(sender, instance, created, **kwargs):
    """
    Handles post creation actions on :py:class:`Website <websites.models.Website>` instances.

    :param sender: sender of the signal
    :param instance: Website instance
    :param created: whether the instance has just been created

    This signal handler starts multiple Celery_ tasks.

    """
    if created:
        task_result = TaskResult.objects.create_task_result(
            "create_web_php_fpm_pool_config",
            create_web_php_fpm_pool_config.s(instance.osuser.username),
        )
        _LOGGER.info(
            "PHP FPM configuration creation has been requested in task %s",
            task_result.task_id,
        )

        fqdn = instance.get_fqdn()
        task_result = TaskResult.objects.create_task_result(
            "create_file_website_hierarchy",
            create_file_website_hierarchy.s(instance.osuser.username, fqdn),
        )
        _LOGGER.info(
            "website file hierarchy creation for %s has been requested in task %s",
            fqdn,
            task_result.task_id,
        )

        task_result = TaskResult.objects.create_task_result(
            "create_web_vhost_config",
            create_web_vhost_config.s(
                instance.osuser.username, fqdn, instance.wildcard
            ),
        )
        _LOGGER.info(
            "virtual host configuration for %s has been requested in task %s",
            fqdn,
            task_result.task_id,
        )

        task_result = TaskResult.objects.create_task_result(
            "enable_web_vhost", enable_web_vhost.s(fqdn)
        )
        _LOGGER.info(
            "virtual host enabling has been requested in task %s", task_result.task_id
        )

    _LOGGER.debug(
        "website %s has been %s", instance, created and "created" or "updated"
    )


@receiver(post_delete, sender=Website)
def handle_website_deleted(sender, instance, **kwargs):
    """
    Handles cleanup actions to be done after deletion of a :py:class:`Website <websites.models.Website>` instance.

    :param sender: sender of the signal
    :param instance: Mailbox instance

    This signal handler starts multiple Celery_ tasks.

    """
    fqdn = instance.get_fqdn()
    os_user = instance.osuser

    task_result = TaskResult.objects.create_task_result(
        "disable_web_vhost", disable_web_vhost.s(fqdn)
    )
    _LOGGER.info(
        "virtual host disabling for %s has been requested in task %s",
        fqdn,
        task_result.task_id,
    )

    task_result = TaskResult.objects.create_task_result(
        "delete_web_vhost_config", delete_web_vhost_config.s(fqdn)
    )
    _LOGGER.info(
        "virtual host configuration deletion for %s has been requested in task %s",
        fqdn,
        task_result.task_id,
    )

    task_result = TaskResult.objects.create_task_result(
        "delete_file_website_hierarchy",
        delete_file_website_hierarchy.s(os_user.username, fqdn),
    )
    _LOGGER.info(
        "website file hierarchy deletion for %s has been requested in task %s",
        fqdn,
        task_result.task_id,
    )

    if not Website.objects.filter(osuser=os_user):
        task_result = TaskResult.objects.create_task_result(
            "delete_web_php_fpm_pool_config",
            delete_web_php_fpm_pool_config.s(os_user.username),
        )
        _LOGGER.info(
            "PHP FPM configuration deletion for OS user %s has been requested in task %s",
            os_user.username,
            task_result.task_id,
        )
