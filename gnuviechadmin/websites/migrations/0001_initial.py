# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("osusers", "0004_auto_20150104_1751"),
        ("domains", "0002_auto_20150124_1909"),
    ]

    operations = [
        migrations.CreateModel(
            name="Website",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "subdomain",
                    models.CharField(max_length=64, verbose_name="sub domain"),
                ),
                (
                    "wildcard",
                    models.BooleanField(default=False, verbose_name="wildcard"),
                ),
                (
                    "domain",
                    models.ForeignKey(
                        verbose_name="domain",
                        to="domains.HostingDomain",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "osuser",
                    models.ForeignKey(
                        verbose_name="operating system user",
                        to="osusers.User",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "website",
                "verbose_name_plural": "websites",
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name="website",
            unique_together={("domain", "subdomain")},
        ),
    ]
