from django.contrib.auth import get_user_model
from django.core.management import BaseCommand

from help.models import HelpUser

User = get_user_model()


class Command(BaseCommand):
    help = "Populate help user information for existing users"

    def handle(self, *args, **options):
        for user in User.objects.filter(helpuser=None):
            help_user = HelpUser.objects.create(user_id=user.id, email_address=user.email)
            help_user.generate_offline_account_code()
            help_user.save()
            self.stdout.write(f"created offline account code {help_user.offline_account_code} for {user}.")
