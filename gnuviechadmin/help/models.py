import uuid

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _


class HelpUser(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    email_address = models.EmailField(help_text=_("Contact email address"))
    postal_address = models.TextField(help_text=_("Contact postal address"), blank=True)
    offline_account_code = models.CharField(
        help_text=_("Offline account reset code"), max_length=36, default=""
    )

    def generate_offline_account_code(self):
        self.offline_account_code = str(uuid.uuid4())
