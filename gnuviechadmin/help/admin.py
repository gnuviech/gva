from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from help.models import HelpUser

User = get_user_model()


class HelpUserInline(admin.StackedInline):
    model = HelpUser
    can_delete = False
    readonly_fields = ("offline_account_code",)


class UserAdmin(BaseUserAdmin):
    inlines = (HelpUserInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
