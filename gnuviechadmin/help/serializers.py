"""
Serializers for the REST API
"""

from rest_framework import serializers

from help.models import HelpUser


class HelpUserSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = HelpUser
        fields = [
            "url",
            "user",
            "email_address",
            "postal_address",
            "offline_account_code",
        ]
        read_only_fields = ["user", "offline_account_code"]
