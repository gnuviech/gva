from rest_framework import generics

from help.models import HelpUser
from help.serializers import HelpUserSerializer


class ListHelpUserAPIView(generics.ListAPIView):
    """
    API endpoint that allows user help profile to be viewed or edited.

    """

    queryset = (
        HelpUser.objects.all().prefetch_related("user").order_by("user__username")
    )
    serializer_class = HelpUserSerializer


class HelpUserAPIView(generics.RetrieveUpdateAPIView):
    """
    API endpoint that allows user help profile to be viewed or edited.

    """

    queryset = HelpUser.objects.all()
    serializer_class = HelpUserSerializer
