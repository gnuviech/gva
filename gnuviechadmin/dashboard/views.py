"""
This module defines the views for the gnuviechadmin customer dashboard.

"""
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import DetailView, TemplateView
from gvacommon.viewmixins import StaffOrSelfLoginRequiredMixin

from hostingpackages.models import CustomerHostingPackage


class UserDashboardView(LoginRequiredMixin, TemplateView):
    """
    This is the user dashboard view.

    """

    template_name = "dashboard/user_dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(UserDashboardView, self).get_context_data(**kwargs)
        context["hosting_packages"] = CustomerHostingPackage.objects.filter(
            customer=self.request.user
        )
        return context
