"""
Tests for :py:mod:`dashboard.views`.

"""
from django import http
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

User = get_user_model()

TEST_USER = "test"
TEST_PASSWORD = "secret"


class UserDashboardViewTest(TestCase):
    def _create_test_user(self):
        self.user = User.objects.create(username=TEST_USER)
        self.user.set_password(TEST_PASSWORD)
        self.user.save()

    def test_user_dashboard_view_anonymous(self):
        User.objects.create(username=TEST_USER)
        response = self.client.get(reverse("customer_dashboard"))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/accounts/login/?next=/")

    def test_user_dashboard_view_logged_in_ok(self):
        self._create_test_user()
        self.assertTrue(self.client.login(username=TEST_USER, password=TEST_PASSWORD))
        response = self.client.get(reverse("customer_dashboard"))
        self.assertEqual(response.status_code, 200)

    def test_user_dashboard_view_logged_in_template(self):
        self._create_test_user()
        self.assertTrue(self.client.login(username=TEST_USER, password=TEST_PASSWORD))
        response = self.client.get(
            reverse("customer_dashboard")
        )
        self.assertTemplateUsed(response, "dashboard/user_dashboard.html")

    def test_user_dashboard_view_logged_in_context_fresh(self):
        self._create_test_user()
        self.assertTrue(self.client.login(username=TEST_USER, password=TEST_PASSWORD))
        response = self.client.get(reverse("customer_dashboard"))
        self.assertIn("hosting_packages", response.context)
        self.assertEqual(len(response.context["hosting_packages"]), 0)
