from __future__ import absolute_import

from django.urls import path

from .views import UserDashboardView

urlpatterns = [
    path("", UserDashboardView.as_view(), name="customer_dashboard"),
]
