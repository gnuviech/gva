"""
Tests for :py:mod:`contact_form.views`.

"""
from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse

User = get_user_model()

TEST_USER = "test"
TEST_PASSWORD = "secret"
TEST_EMAIL = "test@example.org"
TEST_NAME = "Example Tester".split()
TEST_MESSAGE = """
This is a really unimportant test message.
"""


class ContactFormViewTest(TestCase):
    def _setup_user(self, **kwargs):
        return User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD, **kwargs
        )

    def test_get_contact_form_template(self):
        response = self.client.get(reverse("contact_form"))
        self.assertTemplateUsed(response, "contact_form/contact_form.html")

    def test_get_contact_form_anonymous_status(self):
        response = self.client.get(reverse("contact_form"))
        self.assertEqual(response.status_code, 200)

    def test_get_contact_form_anonymous_has_empty_form(self):
        response = self.client.get(reverse("contact_form"))
        self.assertIn("form", response.context)
        form = response.context["form"]
        self.assertEqual(len(form.initial), 0)

    def test_get_contact_form_fields_anonymous(self):
        response = self.client.get(reverse("contact_form"))
        for name in ("name", "email", "body"):
            self.assertIn(name, response.context["form"].fields)

    def test_post_empty_form_template(self):
        response = self.client.post(reverse("contact_form"), {})
        self.assertTemplateUsed(response, "contact_form/contact_form.html")

    def test_post_empty_form_status(self):
        response = self.client.post(reverse("contact_form"), {})
        self.assertEqual(response.status_code, 200)

    def test_post_empty_form_validation_errors(self):
        response = self.client.post(reverse("contact_form"), {})
        self.assertIn("form", response.context)
        form = response.context["form"]
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 3)

    def test_post_empty_form_no_mail(self):
        self.client.post(reverse("contact_form"), {})
        self.assertEqual(len(mail.outbox), 0)

    def test_get_contact_form_logged_in_no_fullname_initial(self):
        self._setup_user()
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(reverse("contact_form"))
        self.assertIn("form", response.context)
        form = response.context["form"]
        self.assertEqual(form.initial, {"name": TEST_USER, "email": TEST_EMAIL})

    def test_get_contact_form_logged_in_fullname_initial(self):
        self._setup_user(first_name=TEST_NAME[0], last_name=TEST_NAME[1])
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(reverse("contact_form"))
        self.assertIn("form", response.context)
        form = response.context["form"]
        self.assertEqual(
            form.initial, {"name": " ".join(TEST_NAME), "email": TEST_EMAIL}
        )

    def test_post_filled_form_anonymous_redirects(self):
        response = self.client.post(
            reverse("contact_form"),
            {"name": TEST_USER, "email": TEST_EMAIL, "body": TEST_MESSAGE},
        )
        self.assertRedirects(response, reverse("contact_success"))

    def test_post_filled_form_anonymous_mail(self):
        self.client.post(
            reverse("contact_form"),
            {"name": TEST_USER, "email": TEST_EMAIL, "body": TEST_MESSAGE},
        )
        self.assertEqual(len(mail.outbox), 1)

    def test_post_filled_form_logged_in_redirects(self):
        self._setup_user(first_name=TEST_NAME[0], last_name=TEST_NAME[1])
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse("contact_form"),
            {"name": " ".join(TEST_NAME), "email": TEST_EMAIL, "body": TEST_MESSAGE},
        )
        self.assertRedirects(response, reverse("contact_success"))

    def test_post_filled_form_logged_in_mail(self):
        self._setup_user(first_name=TEST_NAME[0], last_name=TEST_NAME[1])
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        self.client.post(
            reverse("contact_form"),
            {"name": " ".join(TEST_NAME), "email": TEST_EMAIL, "body": TEST_MESSAGE},
        )
        self.assertEqual(len(mail.outbox), 1)


class ContactSuccessViewTest(TestCase):
    def test_get_template(self):
        response = self.client.get(reverse("contact_success"))
        self.assertTemplateUsed(response, "contact_form/contact_success.html")

    def test_get_status(self):
        response = self.client.get(reverse("contact_success"))
        self.assertEqual(response.status_code, 200)
