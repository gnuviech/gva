"""
URL patterns for the contact_form views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import ContactFormView, ContactSuccessView

urlpatterns = [
    re_path(r"^$", ContactFormView.as_view(), name="contact_form"),
    re_path(r"^success/$", ContactSuccessView.as_view(), name="contact_success"),
]
