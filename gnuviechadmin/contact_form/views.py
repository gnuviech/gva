"""
This module defines the views of the contact_form app.

"""
from __future__ import absolute_import

from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from .forms import ContactForm


class ContactFormView(FormView):
    """
    This is the contact form view.

    """

    form_class = ContactForm
    template_name = "contact_form/contact_form.html"
    success_url = reverse_lazy("contact_success")

    def get_form_kwargs(self, **kwargs):
        kwargs = super(ContactFormView, self).get_form_kwargs(**kwargs)
        kwargs["request"] = self.request
        return kwargs

    def get_initial(self):
        initial = super(ContactFormView, self).get_initial()
        currentuser = self.request.user
        if currentuser.is_authenticated:
            initial["name"] = currentuser.get_full_name() or currentuser.username
            initial["email"] = currentuser.email
        return initial

    def form_valid(self, form):
        form.save(False)
        return redirect(self.get_success_url())


class ContactSuccessView(TemplateView):
    """
    This view is shown after successful contact form sending.

    """

    template_name = "contact_form/contact_success.html"
