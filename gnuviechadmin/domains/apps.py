"""
This module contains the :py:class:`django.apps.AppConfig` instance for the
:py:mod:`domains` app.

"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DomainAppConfig(AppConfig):
    """
    AppConfig for the :py:mod:`domains` app.

    """

    name = "domains"
    verbose_name = _("Domains")
