"""
This module defines views related to domains.

"""
from __future__ import absolute_import

from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.utils.translation import gettext as _
from django.views.generic.edit import CreateView

from hostingpackages.models import CustomerHostingPackage

from .forms import CreateHostingDomainForm
from .models import HostingDomain


class CreateHostingDomain(PermissionRequiredMixin, CreateView):
    """
    This view is used for creating a new HostingDomain instance for an existing
    hosting package.
    """

    model = HostingDomain
    raise_exception = True
    permission_required = 'domains.add_hostingdomain'
    template_name_suffix = "_create"
    form_class = CreateHostingDomainForm

    def _get_hosting_package(self):
        return get_object_or_404(CustomerHostingPackage, pk=int(self.kwargs["package"]))

    def get_form_kwargs(self):
        kwargs = super(CreateHostingDomain, self).get_form_kwargs()
        kwargs["hostingpackage"] = self._get_hosting_package()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CreateHostingDomain, self).get_context_data(**kwargs)
        hosting_package = self._get_hosting_package()
        context.update(
            {"hostingpackage": hosting_package, "customer": hosting_package.customer}
        )
        return context

    def form_valid(self, form):
        hostingdomain = form.save()
        messages.success(
            self.request,
            _("Successfully created domain {domainname}").format(
                domainname=hostingdomain.domain
            ),
        )
        return redirect(self._get_hosting_package())
