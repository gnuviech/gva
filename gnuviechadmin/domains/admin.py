"""
This module registers the model classes defined in :py:mod:`domains.models`
with the django admin site.

"""
from django.contrib import admin

from domains.models import HostingDomain, MailDomain

admin.site.register(MailDomain)
admin.site.register(HostingDomain)
