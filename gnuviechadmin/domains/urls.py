"""
This module defines the URL patterns for domain related views.

"""
from __future__ import absolute_import

from django.urls import re_path

from .views import CreateHostingDomain

urlpatterns = [
    re_path(
        r"^(?P<package>\d+)/create$",
        CreateHostingDomain.as_view(),
        name="create_hosting_domain",
    ),
]
