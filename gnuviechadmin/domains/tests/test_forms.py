"""
Tests for :py:mod:`domains.forms`.

"""
from unittest.mock import MagicMock, Mock, patch

from django.forms import ValidationError
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _

from domains.forms import CreateHostingDomainForm, relative_domain_validator


class RelativeDomainValidatorTest(TestCase):
    def test_valid_domainname(self):
        relative_domain_validator("example.org")

    def test_domain_name_too_long(self):
        with self.assertRaisesMessage(ValidationError, _("host name too long")):
            relative_domain_validator("e" * 255)

    def test_domain_name_part_too_long(self):
        with self.assertRaisesMessage(ValidationError, _("invalid domain name")):
            relative_domain_validator("a" * 64 + ".org")

    def test_domain_name_illegal_characters(self):
        with self.assertRaisesMessage(ValidationError, _("invalid domain name")):
            relative_domain_validator("eXampl3.org")

    def test_domain_name_starts_with_dash(self):
        with self.assertRaisesMessage(ValidationError, _("invalid domain name")):
            relative_domain_validator("-example.org")

    def test_domain_name_ends_with_dash(self):
        with self.assertRaisesMessage(ValidationError, _("invalid domain name")):
            relative_domain_validator("example-.org")


class CreateHostingDomainFormTest(TestCase):
    def test_constructor_needs_hostingpackage(self):
        instance = MagicMock()
        with self.assertRaises(KeyError):
            CreateHostingDomainForm(instance)

    def test_constructor(self):
        hostingpackage = Mock(id=42)
        instance = MagicMock()
        form = CreateHostingDomainForm(instance, hostingpackage=hostingpackage)
        self.assertTrue(hasattr(form, "hosting_package"))
        self.assertEqual(form.hosting_package, hostingpackage)
        self.assertTrue(hasattr(form, "helper"))
        self.assertEqual(
            form.helper.form_action,
            reverse("create_hosting_domain", kwargs={"package": 42}),
        )
        self.assertEqual(len(form.helper.layout.fields), 2)
        self.assertEqual(form.helper.layout.fields[1].name, "submit")

    def test_domain_field_has_relative_domain_validator(self):
        hostingpackage = Mock(id=42)
        instance = MagicMock()
        form = CreateHostingDomainForm(instance, hostingpackage=hostingpackage)
        self.assertIn(relative_domain_validator, form.fields["domain"].validators)

    def test_clean(self):
        hostingpackage = Mock(id=42)
        instance = MagicMock()
        form = CreateHostingDomainForm(
            instance, hostingpackage=hostingpackage, data={"domain": "example.org"}
        )
        self.assertTrue(form.is_valid())
        self.assertIn("hosting_package", form.cleaned_data)
        self.assertEqual(hostingpackage, form.cleaned_data["hosting_package"])

    def test_save(self):
        hostingpackage = Mock(id=42)
        instance = MagicMock()
        form = CreateHostingDomainForm(
            instance, hostingpackage=hostingpackage, data={"domain": "example.org"}
        )
        self.assertTrue(form.is_valid())
        with patch("domains.forms.HostingDomain") as domain:
            form.save()
            domain.objects.create_for_hosting_package.assert_called_with(
                commit=True, **form.cleaned_data
            )
            form.save(commit=False)
            domain.objects.create_for_hosting_package.assert_called_with(
                commit=False, **form.cleaned_data
            )

    def test_save_m2m(self):
        hostingpackage = Mock(id=42)
        instance = MagicMock()
        form = CreateHostingDomainForm(
            instance, hostingpackage=hostingpackage, data={"domain": "example.org"}
        )
        form.save_m2m()
