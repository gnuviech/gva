"""
Tests for :py:mod:`domains.views`.

"""
from unittest.mock import MagicMock, patch

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from domains.views import CreateHostingDomain
from hostingpackages.models import CustomerHostingPackage, HostingPackageTemplate

User = get_user_model()

TEST_USER = "test"
TEST_PASSWORD = "secret"
TEST_EMAIL = "test@example.org"
TEST_NAME = "Example Tester".split()


class CreateHostingDomainTest(TestCase):
    def _setup_hosting_package(self, customer):
        template = HostingPackageTemplate.objects.create(
            name="testpackagetemplate", mailboxcount=0, diskspace=1, diskspace_unit=0
        )
        package = CustomerHostingPackage.objects.create_from_template(
            customer, template, "testpackage"
        )
        with patch("hostingpackages.models.settings") as hmsettings:
            hmsettings.OSUSER_DEFAULT_GROUPS = []
            package.save()
        return package

    def test_get_anonymous(self):
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": 1})
        )
        self.assertEqual(response.status_code, 403)

    def test_get_regular_user(self):
        customer = User.objects.create_user(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        package = self._setup_hosting_package(customer)
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": package.id})
        )
        self.assertEqual(response.status_code, 403)

    def test_get_staff_user(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": package.id})
        )
        self.assertEqual(response.status_code, 200)

    def test_get_template(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": package.id})
        )
        self.assertTemplateUsed(response, "domains/hostingdomain_create.html")

    def test_get_no_package_found(self):
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": 1})
        )
        self.assertEqual(response.status_code, 404)

    def test_get_get_form_kwargs(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        view = CreateHostingDomain(
            request=MagicMock(), kwargs={"package": str(package.id)}
        )
        the_kwargs = view.get_form_kwargs()
        self.assertIn("hostingpackage", the_kwargs)
        self.assertEqual(the_kwargs["hostingpackage"], package)

    def test_get_context_data_has_hosting_package(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": package.id})
        )
        self.assertIn("hostingpackage", response.context)
        self.assertEqual(response.context["hostingpackage"], package)

    def test_get_context_data_has_customer(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.get(
            reverse("create_hosting_domain", kwargs={"package": package.id})
        )
        self.assertIn("customer", response.context)
        self.assertEqual(response.context["customer"], customer)

    def test_form_valid_redirect(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse("create_hosting_domain", kwargs={"package": package.id}),
            data={"domain": "example.org"},
        )
        self.assertRedirects(response, package.get_absolute_url())

    def test_form_valid_message(self):
        customer = User.objects.create_user("customer")
        package = self._setup_hosting_package(customer)
        User.objects.create_superuser(
            TEST_USER, email=TEST_EMAIL, password=TEST_PASSWORD
        )
        self.client.login(username=TEST_USER, password=TEST_PASSWORD)
        response = self.client.post(
            reverse("create_hosting_domain", kwargs={"package": package.id}),
            follow=True,
            data={"domain": "example.org"},
        )
        messages = list(response.context["messages"])
        self.assertEqual(len(messages), 1)
        self.assertEqual("Successfully created domain example.org", str(messages[0]))
