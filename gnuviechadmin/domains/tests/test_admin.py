from django.test import TestCase
from django.urls import reverse


class TestMailDomainAdmin(TestCase):
    def test_admin_for_maildomain(self):
        admin_url = reverse("admin:domains_maildomain_changelist")
        self.assertIsNotNone(admin_url)
