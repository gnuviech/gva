# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("domains", "0002_auto_20150124_1909"),
    ]

    operations = [
        migrations.CreateModel(
            name="DNSComment",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("commenttype", models.CharField(max_length=10, db_column="type")),
                ("modified_at", models.IntegerField()),
                ("comment", models.CharField(max_length=65535)),
                (
                    "customer",
                    models.ForeignKey(
                        verbose_name="customer",
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
        ),
        migrations.RunSQL(
            """ALTER TABLE domains_dnscomment ADD CONSTRAINT c_lowercase_name
             CHECK (((name)::TEXT = LOWER((name)::TEXT)))"""
        ),
        migrations.CreateModel(
            name="DNSCryptoKey",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("flags", models.IntegerField()),
                ("active", models.BooleanField(default=True)),
                ("content", models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name="DNSDomain",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "domain",
                    models.CharField(
                        unique=True, max_length=255, verbose_name="domain name"
                    ),
                ),
                ("master", models.CharField(max_length=128, null=True, blank=True)),
                ("last_check", models.IntegerField(null=True)),
                (
                    "domaintype",
                    models.CharField(
                        max_length=6,
                        db_column="type",
                        choices=[
                            ("MASTER", "Master"),
                            ("SLAVE", "Slave"),
                            ("NATIVE", "Native"),
                        ],
                    ),
                ),
                ("notified_serial", models.IntegerField(null=True)),
                (
                    "customer",
                    models.ForeignKey(
                        verbose_name="customer",
                        blank=True,
                        to=settings.AUTH_USER_MODEL,
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "DNS domain",
                "verbose_name_plural": "DNS domains",
            },
        ),
        migrations.RunSQL(
            """ALTER TABLE domains_dnsdomain ADD CONSTRAINT c_lowercase_name
            CHECK (((domain)::TEXT = LOWER((domain)::TEXT)))"""
        ),
        migrations.CreateModel(
            name="DNSDomainMetadata",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("kind", models.CharField(max_length=32)),
                ("content", models.TextField()),
                (
                    "domain",
                    models.ForeignKey(to="domains.DNSDomain", on_delete=models.CASCADE),
                ),
            ],
        ),
        migrations.CreateModel(
            name="DNSRecord",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        db_index=True, max_length=255, null=True, blank=True
                    ),
                ),
                (
                    "recordtype",
                    models.CharField(
                        max_length=10, null=True, db_column="type", blank=True
                    ),
                ),
                ("content", models.CharField(max_length=65535, null=True, blank=True)),
                ("ttl", models.IntegerField(null=True)),
                ("prio", models.IntegerField(null=True)),
                ("change_date", models.IntegerField(null=True)),
                ("disabled", models.BooleanField(default=False)),
                ("ordername", models.CharField(max_length=255)),
                ("auth", models.BooleanField(default=True)),
                (
                    "domain",
                    models.ForeignKey(to="domains.DNSDomain", on_delete=models.CASCADE),
                ),
            ],
            options={
                "verbose_name": "DNS record",
                "verbose_name_plural": "DNS records",
            },
        ),
        migrations.RunSQL(
            """ALTER TABLE domains_dnsrecord ADD CONSTRAINT c_lowercase_name
            CHECK (((name)::TEXT = LOWER((name)::TEXT)))"""
        ),
        migrations.RunSQL(
            """CREATE INDEX recordorder ON domains_dnsrecord (domain_id,
            ordername text_pattern_ops)"""
        ),
        migrations.CreateModel(
            name="DNSSupermaster",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("ip", models.GenericIPAddressField()),
                ("nameserver", models.CharField(max_length=255)),
                (
                    "customer",
                    models.ForeignKey(
                        verbose_name="customer",
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="DNSTSIGKey",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("algorithm", models.CharField(max_length=50)),
                ("secret", models.CharField(max_length=255)),
            ],
        ),
        migrations.RunSQL(
            """ALTER TABLE domains_dnstsigkey ADD CONSTRAINT c_lowercase_name
            CHECK (((name)::TEXT = LOWER((name)::TEXT)))"""
        ),
        migrations.AlterField(
            model_name="hostingdomain",
            name="domain",
            field=models.CharField(
                unique=True, max_length=255, verbose_name="domain name"
            ),
        ),
        migrations.AlterField(
            model_name="maildomain",
            name="domain",
            field=models.CharField(
                unique=True, max_length=255, verbose_name="domain name"
            ),
        ),
        migrations.AddField(
            model_name="dnscryptokey",
            name="domain",
            field=models.ForeignKey(to="domains.DNSDomain", on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name="dnscomment",
            name="domain",
            field=models.ForeignKey(to="domains.DNSDomain", on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name="dnssupermaster",
            unique_together=set([("ip", "nameserver")]),
        ),
        migrations.AlterUniqueTogether(
            name="dnstsigkey",
            unique_together=set([("name", "algorithm")]),
        ),
        migrations.AlterIndexTogether(
            name="dnsrecord",
            index_together=set([("name", "recordtype")]),
        ),
        migrations.AlterIndexTogether(
            name="dnscomment",
            index_together={("name", "commenttype"), ("domain", "modified_at")},
        ),
    ]
