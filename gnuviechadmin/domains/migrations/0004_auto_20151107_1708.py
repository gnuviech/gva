# -*- coding: utf-8 -*-
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("domains", "0003_auto_20151105_2133"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="dnscomment",
            options={
                "verbose_name": "DNS comment",
                "verbose_name_plural": "DNS comments",
            },
        ),
        migrations.AlterModelOptions(
            name="dnscryptokey",
            options={
                "verbose_name": "DNS crypto key",
                "verbose_name_plural": "DNS crypto keys",
            },
        ),
        migrations.AlterModelOptions(
            name="dnsdomainmetadata",
            options={
                "verbose_name": "DNS domain metadata item",
                "verbose_name_plural": "DNS domain metadata items",
            },
        ),
        migrations.AlterModelOptions(
            name="dnssupermaster",
            options={
                "verbose_name": "DNS supermaster",
                "verbose_name_plural": "DNS supermasters",
            },
        ),
        migrations.AlterModelOptions(
            name="dnstsigkey",
            options={
                "verbose_name": "DNS TSIG key",
                "verbose_name_plural": "DNS TSIG keys",
            },
        ),
        migrations.AlterField(
            model_name="dnsdomainmetadata",
            name="kind",
            field=models.CharField(
                max_length=32,
                choices=[
                    ("ALLOW-DNSUPDATE-FROM", "ALLOW-DNSUPDATE-FROM"),
                    ("ALSO-NOTIFY", "ALSO-NOTIFY"),
                    ("AXFR-MASTER-TSIG", "AXFR-MASTER-TSIG"),
                    ("AXFR-SOURCE", "AXFR-SOURCE"),
                    ("FORWARD-DNSUPDATE", "FORWARD-DNSUPDATE"),
                    ("GSS-ACCEPTOR-PRINCIPAL", "GSS-ACCEPTOR-PRINCIPAL"),
                    ("GSS-ALLOW-AXFR-PRINCIPAL", "GSS-ALLOW-AXFR-PRINCIPAL"),
                    ("LUA-AXFR-SCRIPT", "LUA-AXFR-SCRIPT"),
                    ("NSEC3NARROW", "NSEC3NARROW"),
                    ("NSEC3PARAM", "NSEC3PARAM"),
                    ("PRESIGNED", "PRESIGNED"),
                    ("PUBLISH_CDNSKEY", "PUBLISH_CDNSKEY"),
                    ("PUBLISH_CDS", "PUBLISH_CDS"),
                    ("SOA-EDIT", "SOA-EDIT"),
                    ("SOA-EDIT-DNSUPDATE", "SOA-EDIT-DNSUPDATE"),
                    ("TSIG-ALLOW-AXFR", "TSIG-ALLOW-AXFR"),
                    ("TSIG-ALLOW-DNSUPDATE", "TSIG-ALLOW-DNSUPDATE"),
                ],
            ),
        ),
        migrations.AlterField(
            model_name="dnstsigkey",
            name="algorithm",
            field=models.CharField(
                max_length=50,
                choices=[
                    ("hmac-md5", "HMAC MD5"),
                    ("hmac-sha1", "HMAC SHA1"),
                    ("hmac-sha224", "HMAC SHA224"),
                    ("hmac-sha256", "HMAC SHA256"),
                    ("hmac-sha384", "HMAC SHA384"),
                    ("hmac-sha512", "HMAC SHA512"),
                ],
            ),
        ),
    ]
