# -*- coding: utf-8 -*-
import django.utils.timezone
import model_utils.fields
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("domains", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="HostingDomain",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    "created",
                    model_utils.fields.AutoCreatedField(
                        default=django.utils.timezone.now,
                        verbose_name="created",
                        editable=False,
                    ),
                ),
                (
                    "modified",
                    model_utils.fields.AutoLastModifiedField(
                        default=django.utils.timezone.now,
                        verbose_name="modified",
                        editable=False,
                    ),
                ),
                (
                    "domain",
                    models.CharField(
                        unique=True, max_length=128, verbose_name="domain name"
                    ),
                ),
                (
                    "customer",
                    models.ForeignKey(
                        verbose_name="customer",
                        blank=True,
                        to=settings.AUTH_USER_MODEL,
                        null=True,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "maildomain",
                    models.OneToOneField(
                        null=True,
                        to="domains.MailDomain",
                        blank=True,
                        help_text="assigned mail domain for this domain",
                        verbose_name="mail domain",
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "verbose_name": "Hosting domain",
                "verbose_name_plural": "Hosting domains",
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name="maildomain",
            name="customer",
            field=models.ForeignKey(
                verbose_name="customer",
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name="maildomain",
            name="domain",
            field=models.CharField(
                unique=True, max_length=128, verbose_name="domain name"
            ),
            preserve_default=True,
        ),
    ]
